#!/usr/bin/python3
# -*- coding: utf-8-sig -*-
#
# IBD Filter Creator is a program to calculate a filter based on given
# parametres
# integration with the Subversion source control system.
#
# Copyright (C) 2020 by Matthias Detzler <filter@ing-detzler.de>
#
# IBD Filter Creator is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# IBD Filter Creator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with IBD Filter Creator;  If not, see <http://www.gnu.org/licenses/>.
#
# IBD Filter Creator  uses additional libraries which have their own licence
# For each such librarie you should have received a copy of their respective
# license in a file named module.license
# These modules namely are: wxPython, scipy, numpy, logging, endolith ,logging
# The modules sys, math ,cmath, getopt, os do not seem to have their own
# license so the general python license shall be applied if legal
# You should have received a copy of the GNU General Public License
# along with IBD Filter Creator;  If not, see 
# <https://docs.python.org/3.7/license.html>
import wx as wx
import os, sys
from Backend import netlists, getN, getSatt

def resource_path(relative_path):
	#Get absolute path to resource, works for dev and for PyInstaller
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)

class MainWindow(wx.Frame):
	def __init__(self, parent, title):
		wx.Frame.__init__(self, parent, title=title)
		self.pathname=os.getcwd()
		self.icon = wx.Icon()
		#The Bitmap for the mouseover Info Fields
		bmp = wx.ArtProvider.GetBitmap(wx.ART_INFORMATION)
		#My Logo
		self.icon.CopyFromBitmap(wx.Bitmap(resource_path('./img/logo.ico'), wx.BITMAP_TYPE_ANY))
		self.SetIcon(self.icon)

		#Create and setup the statusbar
		self.statbar=self.CreateStatusBar() # A Statusbar in the bottom of the window
		self.statbar.SetStatusText('')

		# These are the Menues appearing in the Topbar
		filemenu	 = wx.Menu()
		helpmenu	 = wx.Menu()
		aboutmenu	 = wx.Menu()
		###########################################################
		###########################################################
		# Setting up the Menues
		filemenuItem = filemenu.Append(wx.ID_EXIT,"E&xit"," Terminate the program")
		helpmenuItem = helpmenu.Append(wx.ID_INFO,"Report a Bug"," Report a Bug to me ")
		aboutmenuItem = aboutmenu.Append(wx.ID_ABOUT, "&Version","Version information and Contact Link")

		#Eventhandling of the Menubar
		self.Bind(wx.EVT_MENU, self.OnPref, filemenuItem)
		self.Bind(wx.EVT_MENU, self.OnExit, filemenuItem)
		self.Bind(wx.EVT_MENU, self.OnDocs, helpmenuItem)
		self.Bind(wx.EVT_MENU, self.OnReport, helpmenuItem)
		self.Bind(wx.EVT_MENU, self.OnAbout, aboutmenuItem)

		# Creating the menubar.
		menuBar = wx.MenuBar()
		menuBar.Append(filemenu,"&File") # Adding the "filemenu" to the MenuBar
		#menuBar.Append(helpmenu,"&Help") # Adding the "filemenu" to the MenuBar
		menuBar.Append(aboutmenu,"&About") # Adding the "filemenu" to the MenuBar
		self.SetMenuBar(menuBar)  # Adding the MenuBar to the Frame content.

		#creating the buttons
		#First the Gen button with the three checkboxes
		self.cbl = wx.StaticText(self, label='Output Options:')
		self.kiacd = wx.CheckBox(self, label="KiCAD")
		self.ltspice = wx.CheckBox(self, label="LTspice")
		self.plaintxt = wx.CheckBox(self, label="Textfile")
		self.genButt= wx.Button(self, -1, "Create Netlists")
		self.genButt.Bind(wx.EVT_BUTTON, self.OnCreate)
		self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.kiacd)
		self.checkboxes = wx.BoxSizer(wx.VERTICAL)
		self.checkoutcnt = wx.BoxSizer(wx.HORIZONTAL)
		self.checkboxes.Add(self.cbl, 1, wx.EXPAND|wx.ALL)
		self.checkboxes.Add(self.kiacd, 1,wx.EXPAND|wx.ALL)
		self.checkboxes.Add(self.ltspice, 1,wx.EXPAND|wx.ALL)
		self.checkboxes.Add(self.plaintxt, 1, wx.EXPAND|wx.ALL)
		self.checkoutcnt.Add(self.checkboxes, 1, wx.EXPAND|wx.ALL)
		self.checkoutcnt.Add(self.genButt, 1, wx.EXPAND|wx.ALL)
		#A dict with all the Buttons to create
		buttondict 	= {	0:"Get minimun n",
						1:"Calculate stop-band attenuation",
						2:"Save under"}
						#2:"Calculate Passband Attenuation",
		self.butt = wx.BoxSizer(wx.VERTICAL)
		self.buttons = []
		for item in buttondict:
			self.buttons.append(wx.Button(self, -1, buttondict[item]))
			self.butt.Add(self.buttons[item], 1, wx.EXPAND)
		#Bind the Buttons to a event
		self.buttons[0].Bind(wx.EVT_BUTTON, self.OnGetN)
		self.buttons[1].Bind(wx.EVT_BUTTON, self.OnCalcStop)
		#self.buttons[2].Bind(wx.EVT_BUTTON, self.OnCalcPass)
		self.buttons[2].Bind(wx.EVT_BUTTON, self.OnSaveAs)
		self.butt.Add(self.checkoutcnt, 1, wx.EXPAND)
		# the combobox Control
		radiodict = {0:"Filterclass",
					1:"Filtertype",
					2:"Filtertopology",
					3:"Bessel Normalisation"}
		self.classList = 	['Lowpass', 'Highpass', 'Bandpass', 'Bandstop']
		self.typeList = 	['Butterworth', 'Bessel', 'Chebyshev I', 'Chebyshev II', 'Elliptic', 'Legendre']
		self.topologyList = ['Sallen Key', 'MFB', 'Lumped']
		self.besselnorm = ['Phase', 'Magnitude', 'Delay']
		self.radiolist = [self.classList, self.typeList, self.topologyList, self.besselnorm]
		self.radiobuttons = wx.BoxSizer(wx.HORIZONTAL)
		self.radios=[]
		for item in radiodict:
			self.radios.append(wx.RadioBox(self,label=radiodict[item], choices=self.radiolist[item],majorDimension=1,style=wx.RA_SPECIFY_COLS))
			self.radiobuttons.Add(self.radios[item], 1, wx.EXPAND)
		self.radios[0].Bind(wx.EVT_RADIOBOX,self.onRadioClass)
		self.radios[1].Bind(wx.EVT_RADIOBOX,self.onRadioType)
		self.radios[2].Bind(wx.EVT_RADIOBOX,self.onRadioTopo)
		self.png = wx.Image(resource_path('./img/ml-large.png'), wx.BITMAP_TYPE_ANY).ConvertToBitmap()
		self.logo=wx.StaticBitmap(self, -1, self.png, (10, 5), (self.png.GetWidth(), self.png.GetHeight()))
		self.logobx = wx.BoxSizer(wx.VERTICAL)
		self.logobx.Add(self.radiobuttons, 2, wx.EXPAND)
		self.logobx.Add(self.logo, 1, wx.SHAPED|wx.ALL,5)


		#Input Fields
		inputsdict = {	0:"Filterorder 'n': ",
						1:"Cutoff frequency [Hz]:",
						2:"Stop-band Frequency [Hz] :",
						3:"Stop-band Attenuation [dB] :",
						4:"Pass-band Ripple [dB] :",
						5:"Optimize R to [Ohm]:",
						6:"Lower corner frequency [Hz]:",
						7:"Upper corner frequency [Hz]:",
						8:"Target impedance [Ohm]:",
						9:"Output filename:"
						}
		defaultvalue= {	0:"2",
						1:"1k",
						2:"10k",
						3:"60",
						4:"1",
						5:"10k",
						6:"1k",
						7:"2k",
						8:"50",
						9:"MyFilter",
						}

		# the edit control - one line version.
		#self.paralbl = wx.StaticText(self, label="Filter Parameters")
		self.infos = []
		self.entryboxes = wx.BoxSizer(wx.VERTICAL)
		self.infolabel=[]
		self.entrys=[]
		for item in inputsdict:
			self.infos.append(wx.BoxSizer(wx.HORIZONTAL))
			#First the Horizontal Arrangement
			#The descriptive Textlogo
			self.lblname = wx.StaticText(self, label=inputsdict[item], size=[-1,-1])
			#The Mouseover Info Image
			self.infolabel.append(wx.StaticBitmap(self, bitmap=bmp))
			self.infos[item].Add(self.lblname, 10, wx.SHAPED,0)
			self.infos[item].Add(self.infolabel[item], 1, wx.SHAPED,0)
			#The Textinput
			self.entrys.append(wx.TextCtrl(self,value=defaultvalue[item]))
			#Now align them Verticly first the infos then the input
			self.entryboxes.Add(self.infos[item], 1, wx.EXPAND,0)
			self.entryboxes.Add(self.entrys[item], 2, wx.EXPAND,0)
			#At last bind the events
			#self.Bind(wx.EVT_TEXT, self.EvtText, self.entrys[item])
			#self.Bind(wx.EVT_CHAR, self.EvtChar, self.entrys[item])
		self.infolabel[0].Bind(wx.EVT_MOTION, self.on_mouse_overN)
		self.infolabel[1].Bind(wx.EVT_MOTION, self.on_mouse_overFC)
		self.infolabel[2].Bind(wx.EVT_MOTION, self.on_mouse_overPatt)
		self.infolabel[3].Bind(wx.EVT_MOTION, self.on_mouse_overSatt)
		self.infolabel[4].Bind(wx.EVT_MOTION, self.on_mouse_overRip)
		self.infolabel[5].Bind(wx.EVT_MOTION, self.on_mouse_overR)
		self.infolabel[6].Bind(wx.EVT_MOTION, self.on_mouse_overLowC)
		self.infolabel[7].Bind(wx.EVT_MOTION, self.on_mouse_overUpC)
		self.infolabel[8].Bind(wx.EVT_MOTION, self.on_mouse_overTI)
		self.infolabel[9].Bind(wx.EVT_MOTION, self.on_mouse_overOF)

		#Layout
		# Use some sizers to see layout options
		self.sizer = wx.BoxSizer(wx.HORIZONTAL)
		self.sizer.Add(self.logobx, 8,wx.EXPAND|wx.ALL)
		self.sizer.Add(self.entryboxes, 2,wx.EXPAND)
		#self.sizer.Add(self.checkoutcnt, 1,wx.EXPAND)
		self.sizer.Add(self.butt, 2,wx.EXPAND|wx.ALL)

		#Initial Hiding for the default choices:
		self.entrys[4].Hide()#Hide Passbandripple
		self.entrys[6].Hide()#Lower Corner
		self.entrys[7].Hide()#UpperCorner
		self.entrys[8].Hide()#Impedance
		self.radios[3].Hide()
		self.Layout()


		#Layout sizers
		self.SetSizer(self.sizer)
		self.SetAutoLayout(1)
		self.sizer.Fit(self)
		self.Show()

	def OnPref(self, event):
		print('Preferences')
		pass
	def OnExit(self, event):
		self.Close(True)  # Close the frame.
		pass
	def OnDocs(self, event):
		print('Documentation')
		pass
	def OnReport(self, event):
		print('Report a Bug')
		pass
	def OnAbout(self, event):
		# Create a message dialog box
		dlg = wx.MessageDialog(self,"Version 0.1\nGo to: www.ing-detzler.de/ibdfc to get more information.\nIBD Filter Creator is distributed under GPLv3.\nYou should have received a copy of the GNU General Public License\nalong with IBD Filter Creator;  If not, see http://www.gnu.org/licenses/.", "",  wx.OK)
		dlg.ShowModal() # Shows it
		dlg.Destroy() # finally destroy it when finished.
		pass
	def EvtChar(self, event):
		print('EvtChar: %d\n' % event.GetKeyCode())
		pass
	def EvtCheckBox(self, event):
		pass
	def EvtComboBox(self, event):
		pass
	def EvtText(self, event):
		pass

	def OnGetN(self, event):
		ftype=self.radios[1].GetSelection()
		fclass=self.radios[0].GetSelection()
		att=self.entrys[3].GetValue()
		fc=self.entrys[1].GetValue()#Cutoff frequency
		fs=self.entrys[2].GetValue()#Stopband frequency
		rip=self.entrys[4].GetValue()
		lcorner=self.entrys[6].GetValue()#Lower corner
		ucorner=self.entrys[7].GetValue()#upper corner
		self.statbar.SetStatusText('Calculating "n"')
		err,newN=getN(fc,fs,att,ftype,fclass,rip,lcorner,ucorner)
		if err==-1:
			self.statbar.SetStatusText(newN)
			# Create a message dialog box
			self.dlg = wx.MessageDialog(self,newN)
			self.dlg.ShowModal() # Shows it
			self.dlg.Destroy() # finally destroy it when finished.
		else:
			self.entrys[0].SetValue(newN)
		self.statbar.SetStatusText('')
		pass

	def OnCalcStop(self, event):
		ftype=self.radios[1].GetSelection()
		fclass=self.radios[0].GetSelection()
		n=self.entrys[0].GetValue()
		fc=self.entrys[1].GetValue()#Cutoff frequency
		fs=self.entrys[2].GetValue()#Stopband frequency
		rip=self.entrys[4].GetValue()
		lcorner=self.entrys[6].GetValue()#Lower corner
		ucorner=self.entrys[7].GetValue()#upper corner
		self.statbar.SetStatusText('Calculating "stopband attenuation"')
		err,newSatt=getSatt(fc,fs,n,ftype,fclass,rip,lcorner,ucorner)
		if err==-1:
			self.statbar.SetStatusText(newSatt)
			# Create a message dialog box
			self.dlg = wx.MessageDialog(self,newSatt)
			self.dlg.ShowModal() # Shows it
			self.dlg.Destroy() # finally destroy it when finished.
		else:
			self.entrys[3].SetValue(newSatt)
		self.statbar.SetStatusText('')
		pass

	def OnCalcPass(self, event):
		ftype=self.radios[1].GetSelection()
		Satt=self.entrys[3].GetValue()
		order=self.entrys[0].GetValue()
		fc=self.entrys[1].GetValue()#Cutoff frequency
		fs=self.entrys[2].GetValue()#Stopband frequency
		self.statbar.SetStatusText('Calculating "passband attenuation"')
		#newPatt=str(getPatt(fc,fs,order,ftype))
		#self.statbar.SetStatusText('')
		#self.entrys[2].SetValue(newPatt)
		pass

	def OnSaveAs(self, event):

		with wx.DirDialog (self, "Choose input directory", "",
				   wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST) as fileDialog:

			if fileDialog.ShowModal() == wx.ID_CANCEL:
				return     # the user changed their mind
			else:
				self.pathname = fileDialog.GetPath()
				self.statbar.SetStatusText('Output directory set to: ' + self.pathname)
		pass

		#save the current contents in the file
		#self.pathname = self.fileDialog.GetPath()
		#self.statbar.SetStatusText('Output directory set to: ' + self.pathname)
		#pass

	def OnCreate(self, event):
		print('\nEntry Fields:')
		for entry in range(len(self.entrys)):
			print(self.entrys[entry].GetValue())
		print('\nRadio Buttons:')
		for entry in range(len(self.radios)):
			print(self.radios[entry].GetSelection())
		print('\nCheckbuttons:')
		print(self.kiacd.GetValue())
		print(self.ltspice.GetValue())
		print(self.plaintxt.GetValue())
		print('\nOutputpath:')
		print(self.pathname)
		self.statbar.SetStatusText('Calculating filtercomponent values. Please wait')

		pl=[]
		err=0
		#i did not pack those in a loop because i want them to be easily tracable
		#append the parameters in the right order for the backend
		#Radiobuttons
		pl.append(self.radios[0].GetSelection())#Class
		pl.append(self.radios[1].GetSelection())#Filtertype
		pl.append(self.radios[2].GetSelection())#Topology
		pl.append(self.radios[3].GetSelection())#Bessel Normalisaton
		#The entry fields
		pl.append(self.entrys[0].GetValue())#Filter order
		pl.append(self.entrys[1].GetValue())#Cutoff frequency
		pl.append(self.entrys[2].GetValue())#Stopband frequency
		pl.append(self.entrys[3].GetValue())#Stopband attenuation
		pl.append(self.entrys[4].GetValue())#Passband rippel
		pl.append(self.entrys[5].GetValue())#Resistor target
		pl.append(self.entrys[6].GetValue())#Lower corner
		pl.append(self.entrys[7].GetValue())#upper corner
		pl.append(self.entrys[8].GetValue())#target Impedance
		pl.append(self.entrys[9].GetValue())#Desired filename
		#Checkboxes
		pl.append(self.kiacd.GetValue())
		pl.append(self.ltspice.GetValue())
		pl.append(self.plaintxt.GetValue())
		pl.append(self.pathname)# = fileDialog.GetPath()
		#looks ugly but makess the netlist function much more readable in the Backend
		err,emsg=netlists(pl[0],pl[1],pl[2],pl[3],pl[4],pl[5],pl[6],pl[7],pl[8],pl[9],pl[10],pl[11],pl[12],pl[13],pl[14],pl[15],pl[16],pl[17])
		#netlists()
		if err==-1:
			self.statbar.SetStatusText(emsg)
			# Create a message dialog box
			self.dlg = wx.MessageDialog(self,emsg)
			self.dlg.ShowModal() # Shows it
			self.dlg.Destroy() # finally destroy it when finished.
		else:
			self.statbar.SetStatusText('Done! Thank you for using IBD Filter Desinger')
		#i want a string returned or a list of strings to fill a popup window
		#with information if and how to process terminated
		#maybe even an abort function
		
		pass

	def onRadioClass(self, event):
		fclass=self.radios[0].GetSelection()
		fclass=int(fclass)
		if fclass == 0 or fclass == 1:
			#Hides the Upper and lowe Corner Freq entry Fields
			self.entrys[6].Hide()
			self.entrys[7].Hide()
		elif fclass == 2 or fclass == 3:
			#Shows the Upper and lowe Corner Freq entry Fields
			self.entrys[6].Show()
			self.entrys[7].Show()
		else:
			pass
		self.Layout()
		pass

	def onRadioType(self, event):
		ftype=self.radios[1].GetSelection()
		if ftype == 0 or ftype == 5 or ftype == 3:
			#Hides Normalization and Passbandripple entry Field
			self.entrys[4].Hide()
			# hide Normalization
			self.radios[3].Hide()
		elif ftype == 1:
			#Hide Passbandripple entry Field
			self.entrys[4].Hide()
			# TODO:  **show** Normalization
			self.radios[3].Show()
		elif ftype == 2 or ftype == 4:
			# TODO:  **hide** Normalization
			self.radios[3].Hide()
			# Shows the Passbandripple entry Field
			self.entrys[4].Show()
		else:
			pass
		self.Layout()
		pass

	def onRadioTopo(self, event):
		ftopo=self.radios[2].GetSelection()
		if ftopo == 0 or ftopo == 1:
			#Hide the target Impedance
			self.entrys[8].Hide()
		elif ftopo == ftopo == 2:
			#Show the target Impeadance
			self.entrys[8].Show()
		else:
			pass
		self.Layout()
		pass

	def on_mouse_overN(self, event):
		self.infolabel[0].SetToolTip(
			'"n" must be a integer number greater 0.\n'
			'It sets the filter order and hence the sharpness of the responses'
			'"knee" as well as the resulting stop-band attenuation.')
	def on_mouse_overFC(self, event):
		self.infolabel[1].SetToolTip(
			'The cutoff frequency in [Hz]. Must be given as integer'
			', float or in SI notation.\nIt is the frequency at which the '
			'pass-band attenuation is reached.')
	def on_mouse_overPatt(self, event):
		self.infolabel[2].SetToolTip(
			'The stop-band frequency given as integer,'
			'or float in [Hz].\nIt determines when the '
			'stop-band attenuation is reached for the first time')
	def on_mouse_overSatt(self, event):
		self.infolabel[3].SetToolTip(
			'The stop-band attenuation given as integer, or float in [dB].')
	def on_mouse_overRip(self, event):
		self.infolabel[4].SetToolTip(
			'The pass-band ripple given as integer,or float in [dB].\n'
			'Specifies the amount of ripple allowed in the passband.\n'
			'Only available for Chebychev I and Elliptic filters.')
	def on_mouse_overR(self, event):
		self.infolabel[5].SetToolTip(
			'The resistor value in [Ohm]. Must be given as integer, float or in SI notation.'
			'\nThe algorithm tries to match the given value where it is possible.'
			'\nNot available when using lumped filters.')
	def on_mouse_overLowC(self, event):
		self.infolabel[6].SetToolTip(
			'The lower corner frequency in [Hz]. Must be given'
			' as integer, float or in SI notation.\nIt is either the frequency'
			' at which the pass-band is left (band-stop) or entered (band-pass) for'
			' the first time.\nOnly available for band-pass and band-stop filters.')
	def on_mouse_overUpC(self, event):
		self.infolabel[7].SetToolTip(
			'The upper corner frequency in [Hz]. Must be given'
			' as integer, float or in SI notation.\nIt is either the frequency'
			' at which the pass-band is left again (band-pass) or entered '
			'(Bandstop).\nOnly available for band-pass and band-stop filters.')
	def on_mouse_overTI(self, event):
		self.infolabel[8].SetToolTip(
			'The source and load resistance in [Ohm]. Must be given as integer,'
			' float or in SI notation.\n'
			'Only available when using lumped filters.')
	def on_mouse_overOF(self, event):
		self.infolabel[9].SetToolTip('This contains the name of the output file.'
		' Depending on which checkbox you mark on the right a file with this name'
		' and the corresponding ending *.asc, *.sch and/or *.txt will be created'
		' either in the working directory of this program or in the folder you'
		' specified under "Save under"'
		)

FilterTool = wx.App(False) # Create a new app, don't redirect stdout/stderr to a window.
wnd = MainWindow(None, "IBD Filter Creator") 	# A Frame is a top-level window.

FilterTool.MainLoop()
