# IBD Filter Creator

A tool to create electronic filters. The output can be in form of a LTspice 
or KiCAD schematics or just a plain text file.

## Requirements

### Run from executable
Right click the executable file -> Properties -> Permissions
-> Check: **allow executing file as program**

IBD Filter Creator is tested to run under x64 Ubuntu 18.04 and Fedora 31.
There are no additional requirements if you run IBD Filter Creator directly 
from the executable which can be downloaded from my [Website](https://www.ing-detzler.de/ibdfc.html)

### Run from source

The following external libraries/tools need to be installed to run 
IBD Filter Creator from the source code:
- `python3.7` - Usually comes with your Linux distro, on window it has to be installed
- `python3 pip` - apt install python3-pip
- [`scipy`](https://www.scipy.org/) - python3 - pip install --user scipy
   - [`numpy`](https://numpy.org/) - should be part of scipy
- [`wxPython`](https://wxpython.org/) - python3 - pip install --user wxPython
   - wxPython has itself some dependencies.

 
If you have all requirements satisfied just run
```
cd /path/to/location/
python3 ./wxFrontend.py
```
from a lnux terminal to start IBD Filter Creator. The commands under windows should be very similar.

## Usage

If you are using an executable you can run it from anywhere on your system by making it executable (Linux only) and then running it. It will take the program about 20 seconds to start up since all dependencies are bundle into the executable files and it needs to decompress its environment. During that time nothing is displayed at all.

If you are running it from the source, open up a terminal inside the projects main folder and run
"python3 wxFrontend.py"
this should immediately open the Program if all dependencies are correctly resolved.

After opening you should see a selction mask which allows you to enter and select the values needed by the tool to calculate your filter components.
If you have settled on a design you can choose between three output formats, 
- Plain text
- LTspice
- KiCAD

pressing the "Create" button will create a file in the project folder, containing a filter made to your constraints. You can alter the output path with "Save under" to your liking and also rename the file with the entry box "Output filename"


## License
This software is released under the GPLv3. See [LICENSE](LICENSE) for more details.
