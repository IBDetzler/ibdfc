EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 16535 120000
encoding utf-8
Sheet 1 1
Title "Exported Filterdesign by IBD Filter Designer"
Date "today"
Rev "Software v0.1"
Comp "Ingenieurbuero Detzler"
Comment1 ""
Comment2 ""
Comment3 "Keep in mind that these are Ideal Amplifiers"
Comment4 "The result with real Amplifiers will be diffrent"
$EndDescr
$Comp
L Simulation_SPICE:VSIN V1
U 1 1 5E1E006C
P 9750 118900
F 0 "V1" H 9880 118946 50  0000 L CNN
F 1 "VSIN" H 9880 118855 50  0000 L CNN
F 2 "" H 9750 118900 50  0001 C CNN
F 3 "~" H 9750 118900 50  0001 C CNN
F 4 "Y" H 9750 118900 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 9750 118900 50  0001 L CNN "Spice_Primitive"
F 6 "ac 1 0 sin(0 1 1k) dc 0" H 9880 118809 50  0001 L CNN "Spice_Model"
	1    9750 118900
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:VDC V2
U 1 1 5E1E0BBF
P 10250 118900
F 0 "V2" H 10380 118991 50  0000 L CNN
F 1 "VDC" H 10380 118900 50  0000 L CNN
F 2 "" H 10250 118900 50  0001 C CNN
F 3 "~" H 10250 118900 50  0001 C CNN
F 4 "Y" H 10250 118900 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 10250 118900 50  0001 L CNN "Spice_Primitive"
F 6 "dc 5" H 10380 118809 50  0000 L CNN "Spice_Model"
	1    10250 118900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5E1E10BF
P 10250 119150
F 0 "#PWR03" H 10250 118900 50  0001 C CNN
F 1 "GND" H 10255 118977 50  0000 C CNN
F 2 "" H 10250 119150 50  0001 C CNN
F 3 "" H 10250 119150 50  0001 C CNN
	1    10250 119150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5E1E1627
P 10800 119150
F 0 "#PWR01" H 10800 118900 50  0001 C CNN
F 1 "GND" H 10805 118977 50  0000 C CNN
F 2 "" H 10800 119150 50  0001 C CNN
F 3 "" H 10800 119150 50  0001 C CNN
	1    10800 119150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 119150 10800 119100
Wire Wire Line
	10250 119150 10250 119100
Text Label 10250 118700 0    50   ~ 0
V+
Text Label 10800 118700 0    50   ~ 0
V-
$Comp
L power:GND #PWR02
U 1 1 5E1E46BD
P 9750 119150
F 0 "#PWR02" H 9750 118900 50  0001 C CNN
F 1 "GND" H 9755 118977 50  0000 C CNN
F 2 "" H 9750 119150 50  0001 C CNN
F 3 "" H 9750 119150 50  0001 C CNN
	1    9750 119150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 119100 9750 119150
Text Label 9750 118700 0    50   ~ 0
SignalIn
Text Notes 8900 119350 0    50   ~ 0
.ac oct 10k 1 100k
$Comp
L Simulation_SPICE:VDC V3
U 1 1 5E1E1358
P 10800 118900
F 0 "V3" H 10930 118991 50  0000 L CNN
F 1 "VDC" H 10930 118900 50  0000 L CNN
F 2 "" H 10800 118900 50  0001 C CNN
F 3 "~" H 10800 118900 50  0001 C CNN
F 4 "Y" H 10800 118900 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 10800 118900 50  0001 L CNN "Spice_Primitive"
F 6 "dc -5" H 10930 118809 50  0000 L CNN "Spice_Model"
	1    10800 118900
	1    0    0    -1  
$EndComp
