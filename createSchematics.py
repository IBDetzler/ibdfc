#!/usr/bin/python3
# -*- coding: utf-8-sig -*-
#
# IBD Filter Creator is a program to calculate a filter based on given
# parametres
# integration with the Subversion source control system.
#
# Copyright (C) 2020 by Matthias Detzler <filter@ing-detzler.de>
#
# IBD Filter Creator is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# IBD Filter Creator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with IBD Filter Creator;  If not, see <http://www.gnu.org/licenses/>.
#
# IBD Filter Creator  uses additional libraries which have their own licence
# For each such librarie you should have received a copy of their respective
# license in a file named module.license
# These modules namely are: wxPython, scipy, numpy, logging, endolith ,logging
# The modules sys, math ,cmath, getopt, os do not seem to have their own
# license so the general python license shall be applied if legal
# You should have received a copy of the GNU General Public License
# along with IBD Filter Creator;  If not, see 
# <https://docs.python.org/3.7/license.html>
import logging
import math
import sys, getopt, os
argument=str((sys.argv[0]))
logging.basicConfig(filename=argument+'.log',level=logging.WARNING)
global uid #this is used to give each cicad component a unique id
uid=0
def resource_path(relative_path):
	#Get absolute path to resource, works for dev and for PyInstaller
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)


def strip_list_string(element):
#converts the given element to a strings and tries to strip
#the remaining [,] list style elements when possible
    try:
        #FIX
        #This is far from universal and not good pratice
        #the list elements should be accessed directly insted of 
        #stripping them out of their list
        result=''
        result=str(element)#make sure its a string
        result=result.replace('[','')#remove the opening list bracket
        result=result.replace(']','')#remove the closing list bracket
        result=result.replace(',','')#remove the first , you find
        return result
    except Exception as err:
        logging.critical(str(err),'strip_list_string')
        pass
    

def draw_wireelement(element,etype, app):
#this function draws a wire, a junction or a label in either kicad/eeschema format or in LTspice
#these are bundled because they are considered textelements
    try:
        element_string=''#initialize the return variable
        if app=='eeschema':#in case of eeschema
            if etype=='wire':#a component connecting wire
                result=str(element)#make sure its a string
                result=strip_list_string(result)#strip the [],
                #add the string drawing the wire to the return value
                element_string=element_string+'Wire Wire Line\n'+'  '+result+'\n'
            elif etype=='junction':#this is a junction or a dot to connect crossing wires
                result=str(element)#make sure its a string
                result=strip_list_string(result)#strip the [],
                element_string=element_string+'Connection ~ '+result+'\n'
            elif etype=='label':#Textlabels that name the nets
                result=str(element[0])+' '+str(element[1])#append the coordinates for the label
                result=strip_list_string(result)#strip the [],
                #add the string drawing the wire to the return value
                result='Text Label '+result+' 0    50   ~ 0\n'+element[2]
                element_string=element_string+result+'\n'
            else:
                #This is only reached if the etype is unknown eg
                #none of the above
                print('unknown Element', etype)
                return ''
        elif app=='spice':
            if etype=='wire':
                result=str(element)#make sure its a string
                result=strip_list_string(result)#strip the [],
                #add the string drawing the wire to the return value
                element_string=element_string+'WIRE '+result+'\n'
            elif etype=='junction':
                #there are no junctions to be created for ltspice
                pass
            elif etype=='label':
                result=str(element[0])+' '+str(element[1])#append the coordinates for the label
                result=strip_list_string(result)#strip the [],
                #add the string drawing the wire to the return value
                result='FLAG '+result+' '+element[2]
                element_string=element_string+result+'\n'
            else:
                #This is only reached if the etype is unknown eg
                #none of the above
                print('unknown Element', etype)
                return ''
        else:
            #This is only reached if the App is unknown
            print('unknown App')
            return ''
        return element_string
    except Exception as err:
        logging.critical(str(err),'strip_list_string')
        pass

def wires_sk(stagenumber, odd,app):
#This is a set of statice wires which are needed to draw a sallen key architechture
#The function also stores all the positions of the labels as awell as the ones
#of the junctions
    try:
        xgrid=4350#the grid size in x direction on which the componenents are placed
        ygrid=2100#the grid size in y direction on which the componenents are placed
        ygridfactor=3#defines how many sets per row may be drawn befor hopping to the next y value
        

        xgridfactor=0#reset the x position factor
        #this forloop locates the x position of the wireset
        #depending on the stagenumber valid values for x are 0 up to ygridfactor
        #which defines how many "sets" are drawn per row
        for _ in range(0, stagenumber):
            xgridfactor=xgridfactor+1#increment the factor
            if xgridfactor == ygridfactor:#reset x to 0 when it is bigger than the max value
                xgridfactor=0
        #RECENT CHANGE
        #i thought this would throw an error but it seems it doesn't (anymore) so here it goes
        #redefine the y position factore and restoring it in the original by calculating how many sets of threes there are
        ygridfactor=math.floor(stagenumber/ygridfactor)
        # if stagenumber>0:
        #     
        # else:
        #     ygridfactor=0
        #the discrete offset is then calculated by calculating the grid value by its
        #corresponding factor
        x_offset=xgridfactor*xgrid
        y_offset=ygridfactor*ygrid

        
        #These are the base coordinates for the sallen key wires

        wires=[]#reset the wire list
        #a odd set is basically a single pole RC filter which needs diffrent wires 
        #than a regular stage
        if odd==0 :
            #these are the actual wire coordinate on which any even order sk stage is based
            #These coordinates are special to even order stages and only needed there
            wires.append([2500+x_offset, 1900+y_offset, 2850+x_offset, 1900+y_offset])
            wires.append([3250+x_offset, 1100+y_offset, 2850+x_offset, 1100+y_offset])
            wires.append([2850+x_offset, 1100+y_offset, 2850+x_offset, 1900+y_offset])
            wires.append([2850+x_offset, 1900+y_offset, 3250+x_offset, 1900+y_offset])
            wires.append([3550+x_offset, 1100+y_offset, 3800+x_offset, 1100+y_offset])
            wires.append([900 +x_offset, 1900+y_offset, 2200+x_offset, 1900+y_offset])
        else:
            #This is a special wire for odd order stages which have
            #a few lesser wires over all and a longer
            wires.append([900+x_offset,  1900+y_offset, 3250+x_offset, 1900+y_offset])
        #These are wires that are common to both the odd and even satges
        wires.append([5250+x_offset, 1100+y_offset, 5250+x_offset, 1800+y_offset])
        wires.append([5250+x_offset, 1800+y_offset, 4650+x_offset, 1800+y_offset])
        wires.append([4050+x_offset, 1900+y_offset, 3800+x_offset, 1900+y_offset])
        wires.append([3800+x_offset, 2200+y_offset, 3800+x_offset, 1900+y_offset])
        wires.append([3800+x_offset, 1900+y_offset, 3550+x_offset, 1900+y_offset])
        wires.append([4050+x_offset, 1700+y_offset, 3800+x_offset, 1700+y_offset])
        wires.append([3800+x_offset, 1700+y_offset, 3800+x_offset, 1100+y_offset])
        wires.append([3800+x_offset, 1100+y_offset, 5250+x_offset, 1100+y_offset])
        wires.append([3800+x_offset, 2500+y_offset, 3800+x_offset, 2600+y_offset])
        wires.append([5250+x_offset, 1900+y_offset, 5250+x_offset, 1800+y_offset])

        #And these are the coordinates for the junctions
        #Junctions connect crossing wires where wanted
        junctions=[]
        if odd==0 :
            #the even order filters have a additional juctions
            junctions.append([2850+x_offset, 1900+y_offset])
        junctions.append([3800+x_offset, 1900+y_offset])
        junctions.append([3800+x_offset, 1100+y_offset])
        junctions.append([5250+x_offset, 1800+y_offset])

        labels=[]
        #the labels name the nets to connect the stages and make swimulation results
        #easier to understand
        if stagenumber==0:
            #the first stage has a unique label 'signal in' to feed the input in
            labels.append([900+x_offset,  1900+y_offset, 'SignalIn'])
        elif xgridfactor==0:
            #on each new row the last label has to be put onto the input wire
            #of the newly created longer "open end" wire to cennect the
            #two rows
            labels.append([900+x_offset,  1900+y_offset, 'OutStage'+str(stagenumber-1)])
        #These three make the schematics more readable and the simulation result can be 
        #easily identified
        labels.append([5250+x_offset, 1900+y_offset, 'OutStage'+str(stagenumber)])
        labels.append([4250+x_offset, 2100+y_offset, 'V+'])
        labels.append([4250+x_offset, 1500+y_offset, 'V-'])
        #the wire string holds the return vlaue initialize it:
        wire_string=''
        #now draw each wire in the wire list, each junction in the junction 
        #list and each label in the label list
        for wire in wires:
            wire_string=wire_string+draw_wireelement(wire,'wire',app)
        for junction in junctions:
            wire_string=wire_string+draw_wireelement(junction,'junction',app)
        for label in labels:
            wire_string=wire_string+draw_wireelement(label,'label',app)
        return wire_string
    except Exception as err:
            logging.critical(str(err),'failed in wires_sk')
            pass

def wires_mfb(stagenumber, odd,app):
#this function creates the wires labels and junctions nedded to draw a mfb filter stage in 
#eeschema/kicad this is a bit of a diffrent approach rather than writing 
#each string out by hand i only save the coordinates and join them later on. Not sure yet 
# what i prefer, i feel this is shorter but less clear
    try:
        xgrid=4350# the x grid value on which the components are placed
        ygrid=2100# the y grid value on which the components are placed
        #These are the base coordinates for the MFB wires shared by each configuration
        wires=[]
        wires.append([4050, 2100, 3800, 2100])
        wires.append([3800, 2100, 3800, 2600])
        wires.append([4650, 2000, 5250, 2000])
        wires.append([5250, 1100, 5250, 2000])
        wires.append([3550, 1900, 3800, 1900])
        wires.append([3800, 1650, 3800, 1900])
        wires.append([3800, 1900, 4050, 1900])
        wires.append([900 , 1900, 2200, 1900])
        wires.append([2500, 1900, 2850, 1900])
        wires.append([2850, 2150, 2850, 1900])
        wires.append([2850, 1900, 3250, 1900])
        wires.append([3250, 1100, 2850, 1100])
        wires.append([2850, 1100, 2850, 1900])
        wires.append([3550, 1100, 3800, 1100])
        wires.append([3800, 1350, 3800, 1100])
        wires.append([3800, 1100, 5250, 1100])
        wires.append([2850, 2600, 2850, 2450])

        #And these are the coordinates for the junctions
        junctions=[]
        if stagenumber>0:
        #the first stage needs no juction every other stage does, to connect to
        #the previous stage
            junctions.append([900, 1900])
        junctions.append([3800, 1900])
        junctions.append([2850, 1900])
        junctions.append([3800, 1100])

        #These are the labels for each stage which are common to each stage
        labels=[]
        labels.append([5250, 1900, 'OutStage'])
        labels.append([4250, 2300, 'V+'])
        labels.append([4250, 1700, 'V-'])
        #this line modifies the default 'outStage' and adds a number to it
        labels[0][2]=labels[0][2]+str(stagenumber)

        #this is same as the sallen key stage the ygridfactor defiens how many stages per wrow are inserted
        ygridfactor=3
        #calculate the factor for the x offset based on the stagenumber
        xgridfactor=0
        for _ in range(0, stagenumber):
            xgridfactor=xgridfactor+1#increment the factor 
            if xgridfactor == ygridfactor:#reset it to 0 when the max is overflown
                xgridfactor=0
        #think of it as:
        #y=3|x= 
        #   0 1 2
        #   0 1 2
        #   0 1 2
        #much like a table
        #RECENT CHANGE
        #same as in the SK
        ygridfactor=math.floor(stagenumber/ygridfactor)
        # if stagenumber>0:
        #     
        # else:
        #     ygridfactor=0
        #The first stage has a unique label 
        if stagenumber==0:
            labels.append([900,  1900, 'SignalIn'])
        #each following stage has the output of its predecessor as input
        elif xgridfactor==0:
            labels.append([900,  1900, 'OutStage'+str(stagenumber-1)])
        #the discrete offset is then calculated by calculating the grid value by its
        #corresponding factor
        x_offset=xgridfactor*xgrid
        y_offset=ygridfactor*ygrid
        #wire string is the reuturn value, initialize it
        wire_string=''

        for wire in wires:
            #add the calculated offset value to each coordinate
            wire[0]=wire[0]+x_offset
            wire[1]=wire[1]+y_offset
            wire[2]=wire[2]+x_offset
            wire[3]=wire[3]+y_offset
            #make the wire a string and strip the '[],' from it
            result=strip_list_string(wire)
            #for each wire in wires add the wire to the schematic
            wire_string=wire_string+'Wire Wire Line\n'+'  '+result+'\n'

        for junction in junctions:
            #add the calculated offset value to each coordinate
            junction[0]=junction[0]+x_offset
            junction[1]=junction[1]+y_offset
            #make the junction a string and strip the '[],' from it
            result=strip_list_string(junction)
            #for each junction in junctions add the junction to the schematic
            wire_string=wire_string+'Connection ~ '+result+'\n'

        for label in labels:
            #add the calculated offset value to each coordinate
            label[0]=label[0]+x_offset
            label[1]=label[1]+y_offset
            #make the label a string and strip the '[],' from it
            result=str(label[0])+' '+str(label[1])
            result=strip_list_string(result)
            #for each label in labels add the label to the schematic
            result='Text Label '+result+' 0    50   ~ 0\n'+label[2]
            wire_string=wire_string+result+'\n'

        return wire_string
    except Exception as err:
            logging.critical(str(err),'failed in wires_mfb')
            pass

def wires_lum(order,fclass,app):
#this function draws the needed wires for a lumped eeschema filter
#the order defines how many sets of wires are needed
#the fclass defines the set of wires that is needed
#the app has no used function yet, it planned to combine the spice and eeschema functions
    try:
        #elements per row, the elemnts placed in a row befor switching 
        # to the next much like ygridfactor in sk or mf
        epr=15
        ec=1#counts the number of elemnts already used in the stage
        #on bandpass and bansstop filter there are parallel components
        #which have diffrent spacings than the regular components
        if fclass=='bs':
            yspacing=-400#this is the value horizontal components are spaced to each other
            ymargin=800#this is the value horizontal components are spaced to the other components
        elif fclass=='bp':
            order=int(order*2)#bandpassfilters get thei order doubled up because they have two filters of order 'order'
            yspacing=0#these filters have no parallel components so the spacing is 0
            ymargin=0#these filters have no parallel components so the margin is 0
        else:
            yspacing=0#these filters have no parallel components so the spacing is 0
            ymargin=0#these filters have no parallel components so the margin is 0
        #defaults the ofsets to 0
        x_offset=0
        y_offset=0
        #initialize the return value
        wire_string=''
        #this list contains the wire coordinates
        wires=[]
        #the stage id used to labe the stage outputs
        id=1
        #list containing the junction coordinates
        junctions=[]
        #list containing all label coordinates
        labels=[]
        #Add the SignalIn label
        labels.append([900+x_offset, 1900+y_offset, 'SignalIn'])
        wires.append([900 , 1900, 1850, 1900])#wire left side of r source
        wires.append([2150, 1900, 3150, 1900])#wire right side of r source
        if fclass=='bs':
            #on bandstops the gnd wire is shorter than the regular one because components are stacked
            wires.append([3150,  2850, 3150, 2750])#the wire to the gnd flag
            junctions.append([3150+x_offset, 2800+y_offset])#the junction at the gnd net is also further down
        else:
            wires.append([3150,  2600, 3150, 2550])#the wire to the gnd flag
        for element in range(1,order+1):# the one is there to set odd and even counting right, the +1 is there because we dont start at 0
            if element%2==0:
                #horizontal element wires:
                wires.append([3150+x_offset, 1900+y_offset, 3700+x_offset, 1900+y_offset])#the left side wire 
                wires.append([4000+x_offset, 1900+y_offset, 4550+x_offset, 1900+y_offset])#the right side wire
                if fclass=='bs':
                    #horizontal element wires:
                    wires.append([3150+x_offset, 2800+y_offset, 4550+x_offset, 2800+y_offset])#the lower horizontal wire to the sink resistor
                    wires.append([3150+x_offset, 1900+y_offset+yspacing, 3700+x_offset, 1900+y_offset+yspacing])#the left side wire
                    wires.append([4000+x_offset, 1900+y_offset+yspacing, 4550+x_offset, 1900+y_offset+yspacing])#the right side wire
                    wires.append([3150+x_offset, 1900+y_offset, 3150+x_offset, 1500+y_offset])#the left side upper component down connecting wire
                    junctions.append([3150+x_offset, 1500+y_offset])
                else:
                    wires.append([3150+x_offset, 2550+y_offset, 4550+x_offset, 2550+y_offset])#the lower horizontal wire to the sink resistor
                x_offset=x_offset+1400#the next elemtnt will be to the right
            else:
                if fclass=='bs':
                    wires.append([3150+x_offset, 2400+y_offset, 3150+x_offset, 2450+y_offset])#the lower wire
                    wires.append([3150+x_offset, 2100+y_offset, 3150+x_offset, 1900+y_offset])#the upper wire
                    wires.append([3150+x_offset,  2800+y_offset, 3150+x_offset, 2750+y_offset])#the small wire connecting the L to the gnd line
                    junctions.append([3150+x_offset, 2800+y_offset])
                    junctions.append([3150+x_offset, 1900+y_offset])
                    junctions.append([3150+x_offset, 2550+y_offset])
                    junctions.append([3150+x_offset, 2750+y_offset])
                else:
                    #vertical element wires:
                    wires.append([3150+x_offset, 2400+y_offset, 3150+x_offset, 2550+y_offset])#the lower wire
                    wires.append([3150+x_offset, 2100+y_offset, 3150+x_offset, 1900+y_offset])#the upper wire
                    #the corresponding junction dots:
                    junctions.append([3150+x_offset, 1900+y_offset])
                    junctions.append([3150+x_offset, 2550+y_offset])
                    #and the respective label
                #if done with one set of components add the stage out label
                labels.append([3150+x_offset,  1900+y_offset, 'Stage'+str(id)])
                id=id+1
            ec=ec+1#count the elements
            if ec==epr+1:#When the max amount of elemnts per row is reached increase the y offset
                if fclass=='bs':
                    wires.append([3150+x_offset, 1900+y_offset, 3150+x_offset, 1500+y_offset])#the down connecting wire
                    y_offset=y_offset+1250+ymargin# on bandstop add also the extra margin to the y offset
                else:
                    y_offset=y_offset+1250
                x_offset=0#reset the x offset to the first position
                wires.append([950,  1900+y_offset, 3150, 1900+y_offset])#the longer wire at the start of each row
                if fclass=='bs':
                    wires.append([3150,  2850+y_offset, 3150, 2800+y_offset])#wire to the gnd connection
                else:
                    wires.append([3150,  2600+y_offset, 3150, 2550+y_offset])#wire to the gnd connection
                #to connect the two rows we need to name the input of the new row after the
                #output of the old stage
                id=id-1#decrease the counter to match the old stage since it has already been udated
                labels.append([3150+x_offset,  1900+y_offset, 'Stage'+str(id)])#add the label
                id=id+1#set the value back to the correct stage 
                ec=2
        #vertical element wires to connect to the sink resistance:
        if order%2==1 and not order%15==0:
            #in odd order filters that dont end on a linebreak we need a special treatement....
            wires.append([3150+x_offset, 1900+y_offset, 3850+x_offset, 1900+y_offset])#upper sink resistance connection
            wires.append([3150+x_offset, 2550+y_offset, 3850+x_offset, 2550+y_offset])#lower sink resistance connection
            x_offset=x_offset+700
        if fclass == 'bs':
            if order%2==1 and not order%15==0: 
                wires.append([2450+x_offset, 1900+y_offset, 2450+x_offset, 1500+y_offset])#the down connecting wire
            else:
                #when the order is a multiple of 15
                #the source resistor is the first and only elemnt in the row. It needs another wire and junction
                wires.append([3150+x_offset, 1900+y_offset, 3150+x_offset, 1500+y_offset])#the down connecting wire
                junctions.append([3150+x_offset, 1900+y_offset])
            wires.append([3150+x_offset, 2400+y_offset, 3150+x_offset, 2800+y_offset])#the lower wire connecting the sink resistance to gnd
        else:
            wires.append([3150+x_offset, 2400+y_offset, 3150+x_offset, 2550+y_offset])#the lower wire connecting the sink resistance to gnd
        wires.append([3150+x_offset, 2100+y_offset, 3150+x_offset, 1900+y_offset])#the wire connecting the sink resistance to upper components

        #label it:
        if order%2==0:
            #on equal order filter there has to be another label
            labels.append([3150+x_offset, 1900+y_offset, 'SignalOut'])
        else:
            #where on odd order filters the last label is replaced
            labels[-1]=([3150+x_offset, 1900+y_offset, 'SignalOut'])
            #it is at a linebrak rename the one before too
            if order%15==0:
                labels[-2]=([12950, 1900+y_offset-1250-ymargin, 'SignalOut'])
        # x_offset=0
        # y_offset=0
        #Now that all needed coordinates are collected use the specialized functions to 'draw'
        #each wire, junction and label to the schematics
        for wire in wires:
            wire_string=wire_string+draw_wireelement(wire,'wire', 'eeschema')

        for junction in junctions:
            wire_string=wire_string+draw_wireelement(junction,'junction', 'eeschema')

        for label in labels:
            wire_string=wire_string+draw_wireelement(label,'label', 'eeschema')
        return wire_string
    except Exception as err:
            logging.critical(str(err),'failed in wires_lum')
            pass

def create_eeschema_component(name,value,x,y,r):
#this function draws/creates 'components'-strings for eeschema
#it returns a string resembeling the requested componen on position x y
    try:
        #name: the label of the component eg R1
        #value: the value of the component in its respective unit
        #y: y coordinate
        #x: x coordinate
        #r: rotation bit
        etype=name[0]#The first Letter of the name is expected to be the elementtype like R or C
        id=name[1:]#The id is then the rest of the given name
        if etype=='U':#transform the given single letters to the written out forms needed by eeschema
            etype='OPAMP'
        elif etype=='P':
            etype='GND'
        global uid#referenz the gloabl uid counter to make sure each id is only used once
        hexid=hex(uid)#convert it to a hex since that is what eeschema expects
        uid=uid+1#increment the counter since we just used it
        zeros=''#the amount of leading zeros to be added to the uid string
        for _ in range(0,10-len(hexid)):#depending on the current length of hexid a diffrent amount of leading
            zeros=zeros+'0'#zeros is needed - the combined length should be 10 digits
        hexid=hexid.replace('0x',zeros)#the uid is in hex but not marked as such so replace the marking with the leading zeros
        hide='0001'#The value to hide a components textfield
        show='0000'#The value to show a components textfield
        visF0=show#this sets the visibillity for each textfield
        visF1=show
        visF2=hide
        visF3=hide

        if r==0:#this means the component is horizontal so we need to apply the correct orientation to each textlabel
            textID='C'#makes it a standard component
            pattern=' 0    -1    -1    0'#this is the default components 2d orientation patter mirrored/rotated etc
            labelF0='" V '#these set the oriatations of the corresponding label to vertical or horizontal
            labelF1='" V '
            labelF2='" V '
            labelF3='" H '
            #There are two labels visible Name and Value they need a little offset to each other
            xloa=200#sets the x distance A the label has to the component
            xlob=100#sets the x distance B the label has to the component
            yloa=0#sets the y distance A the label has to the component
            yloab=0#sets the y distance B the label has to the component
            #Now redefine certain values depending on the element type
            if etype=='OPAMP':
                pattern=' 1     0     0    1'
                labelF0='" H '
                labelF1='" H '
                labelF2='" V '
                labelF3='" H '
                xloa=150
                xlob=0
                yloa=-100
                yloab=0
            elif etype=='GND':
                pattern=' 1     0     0    -1'
                labelF0='" H '
                labelF1='" H '
                labelF2='" V '
                labelF3='" H '
                visF0=hide
                xloa=0
                xlob=0
                yloa=0
                yloab=-150
            elif etype=='C':
                xloa=250
                xlob=150
                yloa=0
                yloab=0
            else:
                pass

        else:
            textID='R'#this makes a rotated the process is the same as aboce
            pattern=' -1    0    0    1'
            #There are two labels visible Name and Value they need a little offset to each other
            xloa=-50#sets the x distance A the label has to the component
            xlob=-50#sets the x distance B the label has to the component
            yloa=-50#sets the y distance A the label has to the component
            yloab=50#sets the y distance B the label has to the component
            labelF0='" H '#make the labels horizontal
            labelF1='" H '
            labelF2='" V '
            labelF3='" H '
            #again make redefinitions where needed
            if etype=='C':
                xloa=-100
                xlob=-100
                yloa=-50
                yloab=50
            elif etype=='OPAMP':
                pattern=' 1     0     0    -1'
                labelF0='" H '
                labelF1='" H '
                labelF2='" V '
                labelF3='" H '
                xloa=150
                xlob=0
                yloa=-100
                yloab=0
            elif etype=='GND':
                pattern=' 1     0     0    1'
                labelF0='" H '
                labelF1='" H '
                labelF2='" V '
                labelF3='" H '
                visF0=hide
                xloa=0
                xlob=0
                yloa=0
                yloab=-150
            else:
                pass
        #in this part the librarie of the component is set
        #depending on what element type was set
        #there is also an id appended to make the annotation of the component
        if etype=='OPAMP':
            lib='L Simulation_SPICE:'
            id='U'+str(id)

        elif etype=='GND':
            lib='L power:'
            id='#PWR0'+str(id)
        else:
            lib='L Device:'
            id=etype+str(id)
        #these are the needed strings to draw a component
        component='$Comp\n'#each component starts like this
        component=component+lib+etype+' '+id+'\n'#append the librarie and type
        component=component+'U 1 1 '+str(hexid)+'\n'#give it its unique id
        component=component+'P '+str(x)+' '+ str(y)+'\n'#position it on the schematics
        component=component+'F 0 "'+id+labelF0+str(x+xloa)+' '+ str(y+yloa)+' 50  '+visF0+' '+textID+' CNN'+'\n'#make the name
        component=component+'F 1 "'+str(value)+labelF1 +str(x+xlob)+' '+str(y+yloab)+' 50  '+visF1+' '+textID+' CNN'+'\n'#and value
        component=component+'F 2 "'+labelF2+str(x)+' '+ str(y)+' 50  '+visF2+' C CNN'+'\n'
        component=component+'F 3 "~'+labelF3+str(x)+' '+ str(y)+' 50  '+visF3+' C CNN'+'\n'
        #depending on the component also append the correct spice librarie
        if etype=='OPAMP':
            component=component+'F 4 "X" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Primitive"\n'
            component=component+'F 5 "idealOpAmp" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Model"\n'
            component=component+'F 6 "Y" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Netlist_Enabled"\n'
            component=component+'F 7 "idealOpAmp.lib" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Lib_File"\n'
        elif etype=='R':
            component=component+'F 4 "R" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Primitive"\n'
            component=component+'F 5 "'+str(value)+'" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Model"\n'
            component=component+'F 6 "Y" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Netlist_Enabled"\n'
        elif etype=='L':
            component=component+'F 4 "L" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Primitive"\n'
            component=component+'F 5 "'+str(value)+'" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Model"\n'
            component=component+'F 6 "Y" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Netlist_Enabled"\n'
        elif etype=='C':
            component=component+'F 4 "C" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Primitive"\n'
            component=component+'F 5 "'+str(value)+'" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Model"\n'
            component=component+'F 6 "Y" H '+str(x)+' '+ str(y)+' 50  0001 C CNN "Spice_Netlist_Enabled"\n'
        component=component+' 1    '+str(x)+' '+ str(y)+'\n'+pattern+'\n'+'$EndComp'+'\n'#this is how each component ends
        return component
    except Exception as err:
            logging.critical(str(err),'failed in create_eeschema_component')
            pass

def create_spice_component(name,value,x,y,r):
#this function draws/creates 'components'-strings for LTspice
#it returns a string resembeling the requested componen on position x y
    try:
        etype=name[0]#identify which component is requested
        component=''#initialize the component string
        rotation=''#initialize the rotaton
        b='90'#angle of rotation
        if etype=='U':#In case of an OpAmp
            b='0'#OpAmps have currently always the same rotation
            #The next line resembles the whole cmponent in LTspice
            component='SYMBOL Opamps\\UniversalOpamp2 '+str(x)+' '+str(y)+' R'+b+'\n'+rotation+'SYMATTR InstName '+name+'\n'
        elif etype=='P':
            #this is actually a flag/label but since it is a component in KiCAD we go with that in LTspice too
            #to keep things consistent
            component='FLAG '+str(x)+' '+str(y)+' 0'+'\n'
        #The rotation is actually twisted here to match the rotation in eeschema
        #that means a non rotated component is actually a rotated one for LTspice
        elif etype=='R':#A resistor
            if r==0:#Add the additional string needed on rotated components
                rotation='WINDOW 0 0 56 VBottom 2\nWINDOW 3 32 56 VTop 2\n'
            else:#If the rotation bit is set
                rotation=''#make sure the string is empty
                b='0'#redefine the angle to 0
            #assemble the component:
            component='SYMBOL res '+str(x)+' '+str(y)+' R'+b+'\n'+rotation+'SYMATTR InstName '+name+'\n'+'SYMATTR Value '+str(value)+'\n'
        #have a look at R on what this does.
        elif etype=='C':
            if r==0:
                rotation='WINDOW 0 0 32 VBottom 2\nWINDOW 3 32 32 VTop 2\n'
            else:
                rotation=''
                b='0'
            component='SYMBOL cap '+str(x)+' '+str(y)+' R'+b+'\n'+rotation+'SYMATTR InstName '+name+'\n'+'SYMATTR Value '+str(value)+'\n'
        #have a look at R on what this does.
        elif etype=='L':
            if r==0:
                rotation='WINDOW 0 5 56 VBottom 2\nWINDOW 3 32 56 VTop 2\n'
            else:
                rotation=''
                b='0'
            component='SYMBOL ind '+str(x)+' '+str(y)+' R'+b+'\n'+rotation+'SYMATTR InstName '+name+'\n'+'SYMATTR Value '+str(value)+'\n'
        return component
    except Exception as err:
            logging.critical(str(err),'failed in create_spice_component')
            pass

def draw_active_bs(arch,values,stagenumber):
#This function returns a string containing a bandstop stage either in sallen key or mfb architechure
#designed for eeschema.
    try:
        components_string=''#initialize the return value
        components=[]#initialize the component list
        wires=[]#initialize the wire list
        junctions=[]#initialize the junctions
        labels=[]#initialize the labels
        xgrid=4350#the x grid on whoch the stages are placed
        ygrid=2700#the ygrid on whic the stages are placed

        ygridfactor=3#the amount of stages that fit into one row
        xgridfactor=0#the placement of the stage in the row
        for _ in range(0, stagenumber):
            xgridfactor=xgridfactor+1
            if xgridfactor == ygridfactor:
                xgridfactor=0
        ygridfactor=math.floor(stagenumber/ygridfactor)#get the row number and redefine ygridfactor with it
        #calculate the position offsets 
        x_offset=xgridfactor*xgrid
        y_offset=ygridfactor*ygrid

        if arch=='sk':#these are coordinate lists which have a unique component name at position 0
            #lowpass components:
            components.append(['R'+str((stagenumber*4)+1),values[0],2350+x_offset, 1900+y_offset,0])#R1
            components.append(['R'+str((stagenumber*4)+2),values[1],3400+x_offset, 1900+y_offset,0])#R2
            components.append(['C'+str((stagenumber*4)+1),values[2],4250+x_offset, 2500+y_offset,0])#C1
            components.append(['C'+str((stagenumber*4)+2),values[3],3400+x_offset, 1100+y_offset,0])#C2
            #highpass components
            components.append(['C'+str((stagenumber*4)+3),values[4],2350+x_offset, 2500+y_offset,0])#C3
            components.append(['C'+str((stagenumber*4)+4),values[5],3400+x_offset, 2500+y_offset,0])#C4
            components.append(['R'+str((stagenumber*4)+3),values[6],4250+x_offset, 2900+y_offset,0])#R3
            components.append(['R'+str((stagenumber*4)+4),values[7],3400+x_offset, 3100+y_offset,0])#R4
            #opamp and GND
            components.append(['U'+str(stagenumber+1),'',4350+x_offset, 1800+y_offset,0])#OpAmp
            components.append(['P'+str(stagenumber+1),'',4600+x_offset, 2900+y_offset,0])#GND

            #now the wires:
            wires.append([5250+x_offset, 1100+y_offset, 5250+x_offset, 1800+y_offset])
            wires.append([5250+x_offset, 1800+y_offset, 4650+x_offset, 1800+y_offset])
            wires.append([3550+x_offset, 1100+y_offset, 3800+x_offset, 1100+y_offset])
            wires.append([4050+x_offset, 1700+y_offset, 3800+x_offset, 1700+y_offset])
            wires.append([3800+x_offset, 1700+y_offset, 3800+x_offset, 1100+y_offset])
            wires.append([3800+x_offset, 1100+y_offset, 5250+x_offset, 1100+y_offset])
            wires.append([2500+x_offset, 1900+y_offset, 2850+x_offset, 1900+y_offset])
            wires.append([3250+x_offset, 1100+y_offset, 2850+x_offset, 1100+y_offset])
            wires.append([2850+x_offset, 1100+y_offset, 2850+x_offset, 1900+y_offset])
            wires.append([2850+x_offset, 1900+y_offset, 3250+x_offset, 1900+y_offset])
            wires.append([4400+x_offset, 2500+y_offset, 4500+x_offset, 2500+y_offset])
            wires.append([3550+x_offset, 3100+y_offset, 5250+x_offset, 3100+y_offset])
            wires.append([4100+x_offset, 2500+y_offset, 3800+x_offset, 2500+y_offset])
            wires.append([3250+x_offset, 3100+y_offset, 2850+x_offset, 3100+y_offset])
            wires.append([3550+x_offset, 2500+y_offset, 3800+x_offset, 2500+y_offset])
            wires.append([2850+x_offset, 2500+y_offset, 2850+x_offset, 3100+y_offset])
            wires.append([2500+x_offset, 2500+y_offset, 2850+x_offset, 2500+y_offset])
            wires.append([2850+x_offset, 2500+y_offset, 3250+x_offset, 2500+y_offset])
            wires.append([3800+x_offset, 1900+y_offset, 3800+x_offset, 2500+y_offset])
            wires.append([3550+x_offset, 1900+y_offset, 3800+x_offset, 1900+y_offset])
            wires.append([3800+x_offset, 1900+y_offset, 4050+x_offset, 1900+y_offset])
            wires.append([5250+x_offset, 1800+y_offset, 5250+x_offset, 3100+y_offset])
            wires.append([4500+x_offset, 2900+y_offset, 4600+x_offset, 2900+y_offset])
            wires.append([4400+x_offset, 2900+y_offset, 4500+x_offset, 2900+y_offset])
            wires.append([3800+x_offset, 2900+y_offset, 4100+x_offset, 2900+y_offset])
            wires.append([4500+x_offset, 2500+y_offset, 4500+x_offset, 2900+y_offset])
            wires.append([3800+x_offset, 2500+y_offset, 3800+x_offset, 2900+y_offset])
            wires.append([1800+x_offset, 1900+y_offset, 2200+x_offset, 1900+y_offset])
            wires.append([1800+x_offset, 1900+y_offset, 1800+x_offset, 2500+y_offset])
            wires.append([900 +x_offset, 1900+y_offset, 1800+x_offset, 1900+y_offset])
            wires.append([2200+x_offset, 2500+y_offset, 1800+x_offset, 2500+y_offset])

            #and the junction points:
            junctions.append([3800+x_offset, 1100+y_offset])
            junctions.append([2850+x_offset, 1900+y_offset])
            junctions.append([3800+x_offset, 2500+y_offset])
            junctions.append([2850+x_offset, 2500+y_offset])
            junctions.append([3800+x_offset, 1900+y_offset])
            junctions.append([5250+x_offset, 1800+y_offset])
            junctions.append([4500+x_offset, 2900+y_offset])
            junctions.append([1800+x_offset, 1900+y_offset])
            junctions.append([5250+x_offset, 1900+y_offset])

            #and finally the labels:
            if stagenumber==0:# the first stage needs a SignalIn label
                labels.append([900 +x_offset, 1900+y_offset,'SignalIn'])
            elif xgridfactor==0:#stages that start a ne wline need the previous stages output as input
                labels.append([900 +x_offset, 1900+y_offset,'OutStage'+str(stagenumber-1)])
            labels.append([4250+x_offset, 2100+y_offset,'V+'])#OpAmp V+
            labels.append([4250+x_offset, 1500+y_offset,'V-'])#OpAmp V-
            labels.append([5250+x_offset, 1800+y_offset,'OutStage'+str(stagenumber)])#The stages output label

        elif arch=='mfb':#The approach is the same as for the sk but there are diffrent coordinates and components
            #lowpass components:
            components.append(['R'+str((stagenumber*5)+1),values[0],2450+x_offset, 1900+y_offset,0])#R1
            components.append(['R'+str((stagenumber*5)+2),values[1],3400+x_offset, 1900+y_offset,0])#R2
            components.append(['C'+str((stagenumber*5)+1),values[2],3800+x_offset, 1500+y_offset,1])#C1
            components.append(['C'+str((stagenumber*5)+2),values[3],2450+x_offset, 1100+y_offset,0])#C2
            components.append(['R'+str((stagenumber*5)+3),values[4],3400+x_offset, 1100+y_offset,0])#R3
            #highpass components
            components.append(['C'+str((stagenumber*5)+3),values[5],2450+x_offset, 2300+y_offset,0])#C3
            components.append(['C'+str((stagenumber*5)+4),values[6],3400+x_offset, 2300+y_offset,0])#C4
            components.append(['R'+str((stagenumber*5)+5),values[7],2450+x_offset, 2900+y_offset,0])#R5
            components.append(['R'+str((stagenumber*5)+4),values[8],3800+x_offset, 2600+y_offset,1])#R4
            components.append(['C'+str((stagenumber*5)+5),values[9],3400+x_offset, 2900+y_offset,0])#C5
            
            #opamp and GND
            components.append(['U'+str(stagenumber+1),'',4350+x_offset, 2000+y_offset,0])#OpAmp
            components.append(['P'+str(stagenumber+1),'',2150+x_offset, 1100+y_offset,0])#GND
            components.append(['P'+str(stagenumber+1),'',2150+x_offset, 2900+y_offset,0])#GND
            components.append(['P'+str(stagenumber+1),'',4000+x_offset, 2250+y_offset,0])#GND

            wires.append([2150+x_offset, 2900+y_offset, 2300+x_offset, 2900+y_offset])
            wires.append([3800+x_offset, 1350+y_offset, 3800+x_offset, 1100+y_offset])
            wires.append([3550+x_offset, 1100+y_offset, 3800+x_offset, 1100+y_offset])
            wires.append([2150+x_offset, 1100+y_offset, 2300+x_offset, 1100+y_offset])
            wires.append([1900+x_offset, 2300+y_offset, 2300+x_offset, 2300+y_offset])
            wires.append([3250+x_offset, 1100+y_offset, 2850+x_offset, 1100+y_offset])
            wires.append([2600+x_offset, 1100+y_offset, 2850+x_offset, 1100+y_offset])
            wires.append([2850+x_offset, 1100+y_offset, 2850+x_offset, 1900+y_offset])
            wires.append([2600+x_offset, 1900+y_offset, 2850+x_offset, 1900+y_offset])
            wires.append([2850+x_offset, 1900+y_offset, 3250+x_offset, 1900+y_offset])
            wires.append([2600+x_offset, 2300+y_offset, 2850+x_offset, 2300+y_offset])
            wires.append([2850+x_offset, 2300+y_offset, 3250+x_offset, 2300+y_offset])
            wires.append([2850+x_offset, 2900+y_offset, 2850+x_offset, 2300+y_offset])
            wires.append([2600+x_offset, 2900+y_offset, 2850+x_offset, 2900+y_offset])
            wires.append([3250+x_offset, 2900+y_offset, 2850+x_offset, 2900+y_offset])
            wires.append([3550+x_offset, 2900+y_offset, 3800+x_offset, 2900+y_offset])
            wires.append([3800+x_offset, 2750+y_offset, 3800+x_offset, 2900+y_offset])
            wires.append([3800+x_offset, 2300+y_offset, 3800+x_offset, 2450+y_offset])
            wires.append([3550+x_offset, 2300+y_offset, 3800+x_offset, 2300+y_offset])
            wires.append([4050+x_offset, 2100+y_offset, 4000+x_offset, 2100+y_offset])
            wires.append([3800+x_offset, 1650+y_offset, 3800+x_offset, 1900+y_offset])
            wires.append([3550+x_offset, 1900+y_offset, 3800+x_offset, 1900+y_offset])
            wires.append([3800+x_offset, 1900+y_offset, 4050+x_offset, 1900+y_offset])
            wires.append([3800+x_offset, 2900+y_offset, 5250+x_offset, 2900+y_offset])
            wires.append([3800+x_offset, 1100+y_offset, 5250+x_offset, 1100+y_offset])
            wires.append([5250+x_offset, 2900+y_offset, 5250+x_offset, 2000+y_offset])
            wires.append([5250+x_offset, 1100+y_offset, 5250+x_offset, 2000+y_offset])
            wires.append([4650+x_offset, 2000+y_offset, 5250+x_offset, 2000+y_offset])
            wires.append([3800+x_offset, 1900+y_offset, 3800+x_offset, 2300+y_offset])
            wires.append([1900+x_offset, 1900+y_offset, 1900+x_offset, 2300+y_offset])
            wires.append([900 +x_offset, 1900+y_offset, 1900+x_offset, 1900+y_offset])
            wires.append([1900+x_offset, 1900+y_offset, 2300+x_offset, 1900+y_offset])
            wires.append([4000+x_offset, 2100+y_offset, 4000+x_offset, 2250+y_offset])

            junctions.append([2850+x_offset, 1100+y_offset])
            junctions.append([2850+x_offset, 1900+y_offset])
            junctions.append([2850+x_offset, 2300+y_offset])
            junctions.append([2850+x_offset, 2900+y_offset])
            junctions.append([3800+x_offset, 2300+y_offset])
            junctions.append([3800+x_offset, 1900+y_offset])
            junctions.append([3800+x_offset, 2900+y_offset])
            junctions.append([3800+x_offset, 1100+y_offset])
            junctions.append([5250+x_offset, 2000+y_offset])
            junctions.append([1900+x_offset, 1900+y_offset])

            #and finally the labels:
            if stagenumber==0:
                labels.append([900 +x_offset, 1900+y_offset,'SignalIn'])
            elif xgridfactor==0:
                labels.append([900 +x_offset, 1900+y_offset,'OutStage'+str(stagenumber-1)])
            else:
                junctions.append([900+x_offset, 1900+y_offset])
            labels.append([4250+x_offset, 2300+y_offset,'V+'])
            labels.append([4250+x_offset, 1700+y_offset,'V-'])
            labels.append([5250+x_offset, 1800+y_offset,'OutStage'+str(stagenumber)])

        else:#If something elese is requested tell that i dont know what to do
            print('Unknown architechure')
            return ''
        #finaly assemble the stage with the collected coordinates 
        for component in components:
            components_string=components_string+create_eeschema_component(component[0],component[1],component[2],component[3],component[4])
        for wire in wires:
            components_string=components_string+draw_wireelement(wire,'wire','eeschema')
        for junction in junctions:
            components_string=components_string+draw_wireelement(junction,'junction','eeschema')
        for label in labels:
            components_string=components_string+draw_wireelement(label,'label','eeschema')
        return components_string#return the assembled stage
    except Exception as err:
            logging.critical(str(err),'failed in create_active_bs')
            pass

def draw_active_stage(arch,fclass,values,stagenumber,odd,app,labeloffset):
#this function creates regular lowpass and highpass stages in sk or mfb architecture
#since bandpass filters may require a single pole stage at any stagenumber labeloffset is
#needed to avoid annotation issues
    try:
        #arch: the architecture of the filer, mfb sk or lum
        #the fclass of the filter: hp,lp,bp,bs
        #the filterorder
        #values: a list that holds the component values for each stage
        #labeloffset: offsets the label id by the given value
        values=values[1:]#ommit the first entry which contains the stage number
        #the grid on which to place the stages
        xgrid=4350
        ygrid=2100
        x=0#initialize the x value
        y=1#initialize the y value
        r=2#initialize the r value
        lowpass=[]#initialize the lowpass label list
        highpass=[]#initialize the highpass label list
        #These are thes component positions for the Sallen Key
        filter=[]##initialize the filter list
        if odd ==1 :#this will create a simple single rc/cr filter pole 
            arch='sk'#the odd order filter is always a simple volatge follower
            filter.append([3400, 1900,0]) #'R2' in LPsk global position B
            filter.append([3800, 2350,1]) #'C1' in LPsk global position C
            filter.append([4350, 1800,0]) #'U1' in LPsk global position F
            filter.append([3800, 2600,0]) #'GND' in LPsk global position G
            lowpass=['R','C']#append the according lowpass labels
            highpass=['C','R']#append the according highpass labels
            lowpass.append('U')#append the opamp label to both
            highpass.append('U')
            values.append('')#the opamp has a empty value field but it still has to be appended
        else:#this will create either a sk or mfb structure
            #first append the labels to their respective lists
            lowpass=['R','R','C','C']
            highpass=['C','C','R','R']
            #R1 and R2 are also common to both structures so also append them
            filter.append([2350, 1900,0]) #'R1' in LPsk global position A
            filter.append([3400, 1900,0]) #'R2' in LPsk global position B
            if arch=='mfb':
                #C2 and C1 switched to compensate for value position
                if fclass=='lp' and app=='eeschema':
                    filter.append([3800, 1500,1]) #'C2' in LPsk global position D
                    filter.append([2850, 2300,1]) #'C1' in LPsk global position C
                else:
                    filter.append([2850, 2300,1]) #'C1' in LPsk global position C
                    filter.append([3800, 1500,1]) #'C2' in LPsk global position D
                filter.append([3400, 1100,0]) #'R3' global position E
                filter.append([4350, 2000,0]) #'U1' in LPsk global position F
                filter.append([2850, 2600,0]) #'GND' in LPsk global position G
                #These are the Labels for a mfb, appen them
                lowpass.append('R')
                highpass.append('C')
                lowpass.append('U')
                highpass.append('U')
                lowpass.append('P')
                highpass.append('P')
                #the component values are passed down but the OpAmp and ground label need a value tot
                values.append('')
                values.append('')
            else:
                #the sk component coordinates
                filter.append([3800, 2350,1]) #'C1' in LPsk global position C
                filter.append([3400, 1100,0]) #'C2' in LPsk global position D
                filter.append([4350, 1800,0]) #'U1' in LPsk global position F
                lowpass.append('U')
                highpass.append('U')
                values.append('')
                #At last append the power flag and OPAMP
                #so i can just add empty values for them each time.
                #assign the labels according to the filter fclass
            filter.append([3800, 2600,0]) #'GND' in LPsk global position G
        if fclass=='lp':
            labels=lowpass
        elif fclass=='hp':
            labels=highpass
        elif fclass=='bp':
            labels=lowpass+highpass
        else:
            #the bandstop has its own function since its very diffrent to create
            #have a look at 'draw_active_bs'
            pass
        #Append the common gnd to each arch and its value
        labels.append('P')
        values.append('')
        #these are the counters that are used to annotate the components
        rs=0+labeloffset
        cs=0+labeloffset
        us=0
        ps=0
        #identify how many of each components are there per stage
        for label in labels:
            if label.find('R')==0:
                rs=rs+1
            elif label.find('C')==0:
                cs=cs+1
            elif label.find('U')==0:
                us=us+1
            elif label.find('P')==0:
                ps=ps+1

        #subtract one from the R and C ids when stagenumber greater 1
        #and the filterorder is odd. Because in that case its only a rc/cr filter
        #the subtraction is done by initzializing the values diffrently
        if odd == 0 or stagenumber == 0:
            cnumber=1
            rnumber=1
            pnumber=1
            unumber=1
        else:
            cnumber=0
            rnumber=0
            pnumber=1
            unumber=1


        #look at every label and add it's correct identifier
        labelnumber=0
        #identify what label you looking at add its correct annotation and the increase the respective counter
        #to keep track of the current component count
        for _ in labels:
            if labels[labelnumber].find('R')==0:
                labels[labelnumber]=labels[labelnumber]+str(rnumber+rs*stagenumber)
                rnumber=rnumber+1
            elif labels[labelnumber].find('C')==0:
                labels[labelnumber]=labels[labelnumber]+str(cnumber+cs*stagenumber)
                cnumber=cnumber+1
            elif labels[labelnumber].find('U')==0:
                labels[labelnumber]=labels[labelnumber]+str(unumber+us*stagenumber)
                unumber=unumber+1
            elif labels[labelnumber].find('P')==0:
                labels[labelnumber]=labels[labelnumber]+str(pnumber+ps*stagenumber)
                pnumber=pnumber+1
            labelnumber=labelnumber+1#increase the label index
        #calculate the coordinate offsets
        ygridfactor=3#defines how many satges fit in a row
        xgridfactor=0#init the var
        for _ in range(0, stagenumber):
            xgridfactor=xgridfactor+1
            if xgridfactor == ygridfactor:
                xgridfactor=0
        ygridfactor=math.floor(stagenumber/ygridfactor)
        x_offset=xgridfactor*xgrid
        y_offset=ygridfactor*ygrid

        position=0
        components_string=''
        #Draw a component for each label in the label list
        if app=='eeschema':
            for label in labels:
                components_string=components_string+create_eeschema_component(label,values[position],filter[position][x]+x_offset,filter[position][y]+y_offset,filter[position][r])
                position=position+1
        elif app=='spice':
            for label in labels:
                components_string=components_string+create_spice_component(label,values[position],filter[position][x]+x_offset,filter[position][y]+y_offset,filter[position][r])
                position=position+1
        else:
            print ('Unknown App')
            return
        #add the needed wires
        if arch=='sk':
            components_string=components_string+wires_sk(stagenumber, odd,app)
        elif arch=='mfb':
            components_string=components_string+wires_mfb(stagenumber, odd,app)

        return components_string
    except Exception as err:
            logging.critical(str(err),'failed in draw_active_stage')
            pass

def draw_passive_stage(arch,fclass,values,order):
#this function generates the needed coordinates and labels to draw a passive stage in eeschema
    try:
        stage_string=''
        yspacing=-400#the amount of space between the bs pairs
        if fclass=='bs':
            ymargin=800
        else:
            ymargin=0
        epr=19#factor multiplying xgrid to see lineend
        rows=math.ceil((order/14))
        x_offset=0
        y_offset=0
        #values=values[0]
        filter=[]
        labels=[]
        g=1
        if fclass=='hp':
            for k in range(0,order):
                if k%2==1:
                    labels.append('C'+str(g-1))
                else:
                    labels.append('L'+str(g))
                    g=g+1

        elif fclass=='lp':
            for k in range(0,order):
                if k%2==1:
                    labels.append('L'+str(g-1))
                else:
                    labels.append('C'+str(g))
                    g=g+1

        elif fclass=='bp':
            for k in range(0,int(order)):
                if k%2==1:
                    labels.append('L'+str(g))
                    g=g+1
                else:
                    labels.append('C'+str(g))
            for k in range(0,int(order)):
                if k%2==1:
                    labels.append('C'+str(g))
                    g=g+1
                else:
                    labels.append('L'+str(g))
            rows=math.ceil((order/7))
        
        elif fclass=='bs':
            for k in range(0,order):
                labels.append('C'+str(g))
                labels.append('L'+str(g))
                g=g+1
            rows=math.ceil((order/14))

        ne=0#element counter
        filter.append(['R1',values[0][ne],2000, 1900,0])#source resistor
        ne=ne+1
        r=1
        d=0
        cnt=1
        if fclass=='bs':
            for label in labels:
                #cant use r directly here because it is used to rotate the components
                #d defines either C d==0 or L d==1
                #r gives the rotation either horizontal or vertical

                #vertical cap
                if r==1 and d==0: #flip the rotation bit for every unit of two
                    filter.append([label,values[0][cnt],3150+x_offset, 2250+y_offset,r])#vertical components
                    ne=ne+1#
                    d=1

                #vertical inductor
                elif r==1 and d==1:
                    filter.append([label,values[1][cnt+1],3150+x_offset, 2600+y_offset,r])
                    ne=ne+1#
                    cnt=cnt+1
                    r=0
                    d=0
                    x_offset=x_offset+700
                    if 3150+x_offset>700*epr:
                        x_offset=700
                        y_offset=y_offset+1250+ymargin

                #horizontal cap
                elif r==0 and d==0:
                    filter.append([label,values[1][cnt-1],3150+x_offset, 1900+y_offset,r])#horizontal components
                    ne=ne+1#
                    d=1

                #horizontal inductor
                else:
                    filter.append([label,values[0][cnt],3150+x_offset, 1900+y_offset+yspacing,r])#horizontal components
                    ne=ne+1#
                    cnt=cnt+1
                    r=1
                    d=0
                    x_offset=x_offset+700
                    if 3150+x_offset>700*epr:
                        x_offset=700
                        y_offset=y_offset+1450+ymargin
                
        elif fclass=='bp':
            valuesBP=values[0][:-1]+values[1][1:]
            cnt=0
            of=0
            for label in labels:
                if cnt==int(order):
                    of=1
                if r==1: #flip the rotation bit for each component
                    filter.append([label,valuesBP[ne+of],3150+x_offset, 2250+y_offset,r])#vertical components
                    r=0
                    ne=ne+1#increase the component count
                else:
                    filter.append([label,valuesBP[ne-of],3150+x_offset, 1900+y_offset,r])#horizontal components
                    r=1
                    ne=ne+1#increase the component count
                cnt=cnt+1
                x_offset=x_offset+700
                if 3150+x_offset>700*epr:
                    x_offset=700
                    y_offset=y_offset+1250

        elif fclass=='hp':
            for label in labels:
                if r==1: #flip the rotation bit for each component
                    filter.append([label,values[0][ne+1],3150+x_offset, 2250+y_offset,r])#vertical components
                    r=0
                else:
                    filter.append([label,values[0][ne-1],3150+x_offset, 1900+y_offset,r])#horizontal components
                    r=1
                x_offset=x_offset+700
                if 3150+x_offset>700*epr:
                    x_offset=700
                    y_offset=y_offset+1250
                ne=ne+1#increase the component count

        else:
            for label in labels:
                if r==1: #flip the rotation bit for each component
                    filter.append([label,values[0][ne],3150+x_offset, 2250+y_offset,r])#vertical components
                    r=0
                else:
                    filter.append([label,values[0][ne],3150+x_offset, 1900+y_offset,r])#horizontal components
                    r=1
                x_offset=x_offset+700
                if 3150+x_offset>700*epr:
                    x_offset=700
                    y_offset=y_offset+1250
                ne=ne+1#increase the component count

        #If the sinkresistor is at the start of a new line
        #the offset has to be reset. But if the order is 1 it has to stay
        if x_offset==700 and order >1:
            x_offset=0
        filter.append(['R2',values[0][-1],3150+x_offset, 2250+y_offset,1])#sink resistor
        ne=ne+1
        if fclass=='bs':#on bp filters the gnd node has to be placed further down
            for lin in range (0,rows):
                #only apply the margin after the first line
                if lin>0:
                    filter.append(['P'+str(4+lin),'',3150, 2850+ymargin+1250*lin,0])#append a gnd node to each line
                else:
                    filter.append(['P'+str(4+lin),'',3150, 2850+(1250*lin),0])#append a gnd node to each line
                ne=ne+1
        else:
            for lin in range (0,rows):
                #only apply the margin after the first line
                if lin>0:
                    filter.append(['P'+str(4+lin),'',3150, 2600+ymargin+1250*lin,0])#append a gnd node to each line
                else:
                    filter.append(['P'+str(4+lin),'',3150, 2600+(1250*lin),0])#append a gnd node to each line
                ne=ne+1
        for element in filter:
            stage_string=stage_string+create_eeschema_component(element[0],element[1],element[2],element[3],element[4])
        stage_string=stage_string+wires_lum(order,fclass,'eeschema')
        return stage_string
    except Exception as err:
            logging.critical(str(err),'failed in draw_active_stage')
            pass

def draw_spice_sk(arch,fclass,values,stagenumber,odd):
#this function creates the needed coordinates and labels to draw a sk stage in
#LTspice
    try:
        #arch: the architecture of the filer, mfb sk or lum
        #the fclass of the filter: hp,lp,bp,bs
        #the filterorder
        #values: a list that holds the component values for each stage
        #values=values[1:]#ommit the first entry which contains the stage number
        #the grid on which to place the stages
        xgrid=848
        ygrid=496

        ygridfactor=3
        if stagenumber>0:
            ygridfactor=math.floor(stagenumber/ygridfactor)
        else:
            ygridfactor=0

        xgridfactor=0
        for _ in range(0, stagenumber):
            xgridfactor=xgridfactor+1
            if xgridfactor == 3:
                xgridfactor=0

        x_offset=xgridfactor*xgrid
        y_offset=ygridfactor*ygrid
        #a bandpass just cascades stages
        #i don't use this but it's there for completeness.
        if fclass=='bp':
            if (stagenumber+2)%2==0:
                fclass='lp'
            else:
                fclass='hp'
        #lets start with the components:
        #component grid appears to be 16 in ltspice
        components=[]
        wires=[]
        labels=[]
        #in ltspice caps and r's are diffrent length, so i cant reuse the wires.
        if fclass =='lp':
            if odd==0:
                components.append(['R'+str((stagenumber*2)+1),values[0],0  +x_offset, 112+y_offset,0])#R1
                components.append(['R'+str((stagenumber*2)+2),values[1],176+x_offset, 112+y_offset,0])#R2
                components.append(['C'+str((stagenumber*2)+1),values[3],160+x_offset, 0  +y_offset,0])#C1
                components.append(['C'+str((stagenumber*2)+2),values[2],224+x_offset, 160+y_offset,1])#C2
                components.append(['U'+str((stagenumber)+1)  ,'',352+x_offset, 112+y_offset,0])#U1
                components.append(['P'+str((stagenumber)+1)  ,'',240+x_offset, 240+y_offset,0])#GND

                wires.append([  96+x_offset,  16+y_offset,   32+x_offset,  16+y_offset])
                wires.append([ 240+x_offset,  16+y_offset,  160+x_offset,  16+y_offset])
                wires.append([ 480+x_offset,  16+y_offset,  240+x_offset,  16+y_offset])
                wires.append([ 240+x_offset,  96+y_offset,  240+x_offset,  16+y_offset])
                wires.append([ 320+x_offset,  96+y_offset,  240+x_offset,  96+y_offset])
                wires.append([ 480+x_offset, 112+y_offset,  480+x_offset,  16+y_offset])
                wires.append([ 480+x_offset, 112+y_offset,  384+x_offset, 112+y_offset])
                wires.append([ -96+x_offset, 128+y_offset, -288+x_offset, 128+y_offset])
                wires.append([  32+x_offset, 128+y_offset,   32+x_offset,  16+y_offset])
                wires.append([  32+x_offset, 128+y_offset,  -16+x_offset, 128+y_offset])
                wires.append([  80+x_offset, 128+y_offset,   32+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 128+y_offset,  160+x_offset, 128+y_offset])
                wires.append([ 320+x_offset, 128+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 160+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 240+y_offset,  240+x_offset, 224+y_offset])

            elif odd==1:
                #FIX
                #this capures the bp labeling issue when creating odd order filters
                if stagenumber ==0:
                    labelnumber=0
                else:
                    labelnumber=1
                components.append(['R'+str((labelnumber)+1),values[0],176+x_offset, 112+y_offset,0])#R1
                components.append(['C'+str((labelnumber)+1),values[1],224+x_offset, 160  +y_offset,1])#C1
                components.append(['U'+str((stagenumber)+1)  ,'',352+x_offset, 112+y_offset,0])#U1
                components.append(['P'+str((stagenumber)+1)  ,'',240+x_offset, 240+y_offset,0])#GND

                wires.append([ 480+x_offset,  16+y_offset,  240+x_offset,  16+y_offset])
                wires.append([ 240+x_offset,  96+y_offset,  240+x_offset,  16+y_offset])
                wires.append([ 320+x_offset,  96+y_offset,  240+x_offset,  96+y_offset])
                wires.append([ 480+x_offset, 112+y_offset,  480+x_offset,  16+y_offset])
                wires.append([ 480+x_offset, 112+y_offset,  384+x_offset, 112+y_offset])
                wires.append([ 240+x_offset, 128+y_offset,  160+x_offset, 128+y_offset])
                wires.append([ 320+x_offset, 128+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 160+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 240+y_offset,  240+x_offset, 224+y_offset])
                wires.append([  80+x_offset, 128+y_offset, -288+x_offset, 128+y_offset])

        elif fclass=='hp':
            if odd==0:
                components.append(['C'+str((stagenumber*2)+1),values[0],-32+x_offset, 112+y_offset,0])#C1
                components.append(['C'+str((stagenumber*2)+2),values[1],144+x_offset, 112+y_offset,0])#C2
                components.append(['R'+str((stagenumber*2)+1),values[2],224+x_offset, 144  +y_offset,1])#R1
                components.append(['R'+str((stagenumber*2)+2),values[3],176+x_offset, 0+y_offset,0])#R2
                components.append(['U'+str((stagenumber)+1)  ,'',352+x_offset, 112+y_offset,0])#U1
                components.append(['P'+str((stagenumber)+1)  ,'',240+x_offset, 256+y_offset,0])#GND

                wires.append([  80+x_offset,  16+y_offset,   32+x_offset,  16+y_offset])
                wires.append([ 240+x_offset,  16+y_offset,  160+x_offset,  16+y_offset])
                wires.append([ 480+x_offset,  16+y_offset,  240+x_offset,  16+y_offset])
                wires.append([ 240+x_offset,  96+y_offset,  240+x_offset,  16+y_offset])
                wires.append([ 320+x_offset,  96+y_offset,  240+x_offset,  96+y_offset])
                wires.append([ 480+x_offset, 112+y_offset,  480+x_offset,  16+y_offset])
                wires.append([ 480+x_offset, 112+y_offset,  384+x_offset, 112+y_offset])
                wires.append([ -96+x_offset, 128+y_offset, -288+x_offset, 128+y_offset])
                wires.append([  32+x_offset, 128+y_offset,   32+x_offset,  16+y_offset])
                wires.append([  32+x_offset, 128+y_offset,  -32+x_offset, 128+y_offset])
                wires.append([  80+x_offset, 128+y_offset,   32+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 128+y_offset,  144+x_offset, 128+y_offset])
                wires.append([ 320+x_offset, 128+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 160+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 256+y_offset,  240+x_offset, 240+y_offset])

            elif odd==1:
                #FIX
                #this capures the bp labeling issue when creating odd order filters
                if stagenumber ==0:
                    labelnumber=0
                else:
                    labelnumber=1
                components.append(['C'+str((labelnumber)+1),values[1],144+x_offset, 112+y_offset,0])#C1
                components.append(['R'+str((labelnumber)+1),values[0],224+x_offset, 144  +y_offset,1])#R1
                components.append(['U'+str((stagenumber)+1)  ,'',352+x_offset, 112+y_offset,0])#U1
                components.append(['P'+str((stagenumber)+1)  ,'',240+x_offset, 256+y_offset,0])#GND

                wires.append([ 480+x_offset,  16+y_offset,  240+x_offset,  16+y_offset])
                wires.append([ 240+x_offset,  96+y_offset,  240+x_offset,  16+y_offset])
                wires.append([ 320+x_offset,  96+y_offset,  240+x_offset,  96+y_offset])
                wires.append([ 480+x_offset, 112+y_offset,  480+x_offset,  16+y_offset])
                wires.append([ 480+x_offset, 112+y_offset,  384+x_offset, 112+y_offset])
                wires.append([  80+x_offset, 128+y_offset, -288+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 128+y_offset,  144+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 160+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 320+x_offset, 128+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 256+y_offset,  240+x_offset, 240+y_offset])


        elif fclass=='bs':#TODO place the right coordinates for the highpass components
            if odd==0:

                components.append(['R'+str((stagenumber*4)+1),values[0],0  +x_offset, 112+y_offset,0])#R1
                components.append(['R'+str((stagenumber*4)+2),values[1],176+x_offset, 112+y_offset,0])#R2
                components.append(['C'+str((stagenumber*4)+1),values[3],160+x_offset, 0  +y_offset,0])#C1
                components.append(['C'+str((stagenumber*4)+2),values[2],368+x_offset, 224+y_offset,0])#C2
                components.append(['C'+str((stagenumber*4)+3),values[4],-32+x_offset, 224  +y_offset,0])#C3
                components.append(['C'+str((stagenumber*4)+4),values[5],160+x_offset, 224+y_offset,0])#C4
                components.append(['R'+str((stagenumber*4)+3),values[7],176  +x_offset, 384+y_offset,0])#R3
                components.append(['R'+str((stagenumber*4)+4),values[6],400+x_offset, 320+y_offset,0])#R4
                components.append(['U'+str((stagenumber)+1)  ,'',352+x_offset, 112+y_offset,0])#U1
                components.append(['P'+str((stagenumber)+1)  ,'',432+x_offset, 336+y_offset,0])#GND

                #the wires:
                wires.append([  96+x_offset,  16+y_offset,   32+x_offset,  16+y_offset])
                wires.append([ 240+x_offset,  16+y_offset,  160+x_offset,  16+y_offset])
                wires.append([ 480+x_offset,  16+y_offset,  240+x_offset,  16+y_offset])
                wires.append([ 240+x_offset,  96+y_offset,  240+x_offset,  16+y_offset])
                wires.append([ 320+x_offset,  96+y_offset,  240+x_offset,  96+y_offset])
                wires.append([ 480+x_offset, 112+y_offset,  480+x_offset,  16+y_offset])
                wires.append([ 480+x_offset, 112+y_offset,  384+x_offset, 112+y_offset])
                wires.append([-160+x_offset, 128+y_offset, -288+x_offset, 128+y_offset])
                wires.append([ -96+x_offset, 128+y_offset, -160+x_offset, 128+y_offset])
                wires.append([  32+x_offset, 128+y_offset,   32+x_offset,  16+y_offset])
                wires.append([  32+x_offset, 128+y_offset,  -16+x_offset, 128+y_offset])
                wires.append([  80+x_offset, 128+y_offset,   32+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 128+y_offset,  160+x_offset, 128+y_offset])
                wires.append([ 320+x_offset, 128+y_offset,  240+x_offset, 128+y_offset])
                wires.append([-160+x_offset, 240+y_offset, -160+x_offset, 128+y_offset])
                wires.append([ -96+x_offset, 240+y_offset, -160+x_offset, 240+y_offset])
                wires.append([  32+x_offset, 240+y_offset,  -32+x_offset, 240+y_offset])
                wires.append([  96+x_offset, 240+y_offset,   32+x_offset, 240+y_offset])
                wires.append([ 240+x_offset, 240+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 240+y_offset,  160+x_offset, 240+y_offset])
                wires.append([ 304+x_offset, 240+y_offset,  240+x_offset, 240+y_offset])
                wires.append([ 416+x_offset, 240+y_offset,  368+x_offset, 240+y_offset])
                wires.append([ 240+x_offset, 336+y_offset,  240+x_offset, 240+y_offset])
                wires.append([ 304+x_offset, 336+y_offset,  240+x_offset, 336+y_offset])
                wires.append([ 416+x_offset, 336+y_offset,  416+x_offset, 240+y_offset])
                wires.append([ 416+x_offset, 336+y_offset,  384+x_offset, 336+y_offset])
                wires.append([ 432+x_offset, 336+y_offset,  416+x_offset, 336+y_offset])
                wires.append([  32+x_offset, 400+y_offset,   32+x_offset, 240+y_offset])
                wires.append([  80+x_offset, 400+y_offset,   32+x_offset, 400+y_offset])
                wires.append([ 480+x_offset, 400+y_offset,  480+x_offset, 112+y_offset])
                wires.append([ 480+x_offset, 400+y_offset,  160+x_offset, 400+y_offset])
            elif odd==1:
                msg='Sorry no odd order band-stop filters are possible at the moment'
                return msg
        #These are always the same, its the stagecorner and labels
        wires.append([ 480+x_offset, 128+y_offset, 480+x_offset, 112+y_offset])
        wires.append([ 560+x_offset, 128+y_offset, 480+x_offset, 128+y_offset])

        labels=[]
        labels.append([ 560+x_offset, 128+y_offset, 'OutStage'+str(stagenumber)])
        labels.append([ 352+x_offset,  80+y_offset, 'V+'])
        labels.append([ 352+x_offset, 144+y_offset, 'V-'])
        if stagenumber==0:
            labels.append([-288+x_offset, 128+y_offset, 'SignalIn'])
        elif xgridfactor==0:
            labels.append([ -288+x_offset, 128+y_offset, 'OutStage'+str(stagenumber-1)])

        stage=''
        for component in components:
            stage=stage+create_spice_component(component[0],component[1],component[2],component[3],component[4])
        for wire in wires:
            stage=stage+draw_wireelement(wire,'wire','spice')
        for label in labels:
            stage=stage+draw_wireelement(label,'label','spice')

        return stage
    except Exception as err:
        logging.critical(str(err),'failed in draw_spice_sk')
        pass

def draw_spice_lum(fclass,values,order):
#This function draws the wires for a lumped filter in LTspice
    try:
        #all lumped filters start with their head containing
        #source resistance ground flag and some wires as well
        #as the SignalIn FLAG
        xgrid=320
        x_offset=0
        y_offset=0#for future use
        yspacing=160
        wires=[]
        labels=[]
        components=[]
        #this is needed by every filter, its the source resistor and misc,stuff
        components.append(['R1',values[0][0],-160 +x_offset, 112+y_offset,0])#Source resistor
        wires.append([ -256+x_offset, 128+y_offset, -288+x_offset, 128+y_offset])
        wires.append([ -96+x_offset, 128+y_offset, -176+x_offset, 128+y_offset])
        labels.append([-288+x_offset, 128+y_offset, 'SignalIn'])
        
        flipper=0
        if fclass== 'hp':
            x_offset=x_offset+xgrid
            c_rotate=0
            l_rotate=1
        else:
            c_rotate=1
            l_rotate=0

        l_count=1
        c_count=1
        flipper=0
        if fclass=='lp' or fclass =='hp':
            for element in range(order):
                if flipper==0:#thats a cap:
                    #depending on which orientation the cap has it needs a gnd and diffrent wires, or not
                    if c_rotate==1:
                        #cap is verical:
                        components.append(['C'+str(c_count),values[0][element+1],-112+x_offset, 160  +y_offset,1])#C1
                        components.append(['P1','',-96+x_offset, 272+y_offset, 0])#The GND label
                        wires.append([-96+x_offset, 160+y_offset, -96+x_offset, 128+y_offset])
                        wires.append([-96+x_offset, 272+y_offset, -96+x_offset, 224+y_offset])
                    else:
                        #cap is horizontal:
                        components.append(['C'+str(c_count),values[0][element+1],-64+x_offset, 112  +y_offset,0])#C1
                        wires.append([-128+x_offset, 128+y_offset, -416+x_offset, 128+y_offset])
                        wires.append([224+x_offset, 128+y_offset,  -64+x_offset, 128+y_offset])
                        pass
                    c_count=c_count+1
                    flipper=1
                else: #thats a inductor
                    #depending on which orientation the cap has it needs a gnd and diffrent wires, or not
                    if l_rotate==1:
                        #inductor is vertical:
                        components.append(['L'+str(l_count),values[0][element+1],-112+x_offset, 224  +y_offset,1])#L1+64
                        components.append(['P1','',-96+x_offset, 368+y_offset, 0])#The GND label
                        wires.append([ -96+x_offset, 240+y_offset, -96+x_offset, 128+y_offset])#the upper wire
                        wires.append([ -96+x_offset, 368+y_offset, -96+x_offset, 320+y_offset])

                    else:
                        #inductor is horizontal:
                        components.append(['L'+str(l_count),values[0][element+1],-32+x_offset, 112+y_offset,0])#L1
                        wires.append([ -128+x_offset, 128+y_offset, -416+x_offset, 128+y_offset])
                        wires.append([ 224+x_offset, 128+y_offset,  -48+x_offset, 128+y_offset])
                    l_count=l_count+1
                    flipper=0
                x_offset=x_offset+xgrid#push every element to the right
        elif fclass=='bs':
            for element in range(order):
                if flipper==0:#thats a vertical pair:

                    #inductor is vertical:
                    #x_offset=x_offset+xspacing#push the inductor a bit to the right
                    components.append(['L'+str(l_count),values[1][element+2],-112+x_offset, 256  +y_offset,1])#L1+64
                    components.append(['P1','',-96+x_offset, 368+y_offset, 0])#The GND label
                    #wires.append([ -96+x_offset, 240+y_offset, -96+x_offset, 128+y_offset])#the upper wire
                    wires.append([ -96+x_offset, 368+y_offset, -96+x_offset, 352+y_offset])
                    #x_offset=x_offset-xspacing#reset the xoffset

                    #depending on which orientation the cap has it needs a gnd and diffrent wires, or not
                    #cap is verical:
                    #x_offset=x_offset-xspacing#push it bit to the left to let room left for the inductor
                    components.append(['C'+str(c_count),values[0][element+1],-112+x_offset, 160  +y_offset,1])#C1
                    wires.append([-96+x_offset, 160+y_offset, -96+x_offset, 128+y_offset])
                    wires.append([-96+x_offset, 272+y_offset, -96+x_offset, 224+y_offset])

                    #the top connection of the two components:
                    wires.append([-48+x_offset, 128+y_offset, -144+x_offset, 128+y_offset])
                    
                    l_count=l_count+1
                    c_count=c_count+1
                    flipper=1
                else: #thats a horizontal pair

                    #inductor is horizontal:
                    y_offset=y_offset-yspacing#push the inmductor to the top
                    components.append(['L'+str(l_count),values[0][element+1],-32+x_offset, 144+y_offset,0])#L1
                    wires.append([ -128+x_offset, 160+y_offset, -368+x_offset, 160+y_offset])
                    wires.append([ 176+x_offset, 160+y_offset,  -48+x_offset, 160+y_offset])
                    y_offset=y_offset+yspacing#reset the yoffset

                    #cap is horizontal:
                    components.append(['C'+str(c_count),values[1][element],-64+x_offset, 112  +y_offset,0])#C1
                    wires.append([-128+x_offset, 128+y_offset, -368+x_offset, 128+y_offset])
                    wires.append([176+x_offset, 128+y_offset,  -64+x_offset, 128+y_offset])

                    #the downconnecting wires:
                    wires.append([-368+x_offset, 128+y_offset, -368+x_offset, 0+y_offset])
                    wires.append([176+x_offset, 128+y_offset, 176+x_offset, 0+y_offset])

                    c_count=c_count+1
                    l_count=l_count+1
                    flipper=0
                x_offset=x_offset+xgrid#push every elemnt to the right
            x_offset=x_offset-48#the last element is pushed too far to the right with the bs setup...idk why this is a fast #FIX
        
        elif fclass=='bp':
            index=1
            for element in range(order):#The given order applies to both the highpass and the lowpass so we have to doble up here
                #start with the lowpass arrangement
                #invert the bits half way through to continue with 
                #the opposite of with what was started eg either a lp or hp
                if element==int((order/2)):
                    index=1
                    c_rotate=not c_rotate
                    l_rotate=not c_rotate
                    flipper=1#switch to the hp arrangement
                if flipper==0:
                    if c_rotate==1:
                        #cap is verical:
                        components.append(['C'+str(c_count),values[0][index],-112+x_offset, 160  +y_offset,1])#C1
                        components.append(['P1','',-96+x_offset, 272+y_offset, 0])#The GND label
                        wires.append([-96+x_offset, 160+y_offset, -96+x_offset, 128+y_offset])
                        wires.append([-96+x_offset, 272+y_offset, -96+x_offset, 224+y_offset])
                        index=index+1
                    else:
                        #cap is horizontal:
                        components.append(['C'+str(c_count),values[1][index],-64+x_offset, 112  +y_offset,0])#C1
                        wires.append([-128+x_offset, 128+y_offset, -416+x_offset, 128+y_offset])
                        wires.append([224+x_offset, 128+y_offset,  -64+x_offset, 128+y_offset])
                        index=index+1
                    c_count=c_count+1
                    x_offset=x_offset+xgrid#push next element to the right
                    #depending on which orientation the cap has it needs a gnd and diffrent wires, or not
                    if l_rotate==1:
                        #inductor is vertical:
                        components.append(['L'+str(l_count),values[1][index],-112+x_offset, 224  +y_offset,1])#L1+64
                        components.append(['P1','',-96+x_offset, 368+y_offset, 0])#The GND label
                        wires.append([ -96+x_offset, 240+y_offset, -96+x_offset, 128+y_offset])#the upper wire
                        wires.append([ -96+x_offset, 368+y_offset, -96+x_offset, 320+y_offset])
                        index=index+1
                    else:
                        #inductor is horizontal:
                        components.append(['L'+str(l_count),values[0][index],-32+x_offset, 112+y_offset,0])#L1
                        wires.append([ -128+x_offset, 128+y_offset, -416+x_offset, 128+y_offset])
                        wires.append([ 224+x_offset, 128+y_offset,  -48+x_offset, 128+y_offset])
                        index=index+1
                    l_count=l_count+1
                    x_offset=x_offset+xgrid#push next element to the right

                elif flipper==1:
                    #depending on which orientation the cap has it needs a gnd and diffrent wires, or not
                    if l_rotate==1:
                        #inductor is vertical:
                        components.append(['L'+str(l_count),values[1][index+1],-112+x_offset, 224  +y_offset,1])#L1+64
                        components.append(['P1','',-96+x_offset, 368+y_offset, 0])#The GND label
                        wires.append([ -96+x_offset, 240+y_offset, -96+x_offset, 128+y_offset])#the upper wire
                        wires.append([ -96+x_offset, 368+y_offset, -96+x_offset, 320+y_offset])
                        index=index+1
                    else:
                        #inductor is horizontal:
                        components.append(['L'+str(l_count),values[0][index+1],-32+x_offset, 112+y_offset,0])#L1
                        wires.append([ -128+x_offset, 128+y_offset, -416+x_offset, 128+y_offset])
                        wires.append([ 224+x_offset, 128+y_offset,  -48+x_offset, 128+y_offset])
                        index=index+1
                    l_count=l_count+1
                    x_offset=x_offset+xgrid#push next element to the right
                    if c_rotate==1:
                        #cap is verical:
                        components.append(['C'+str(c_count),values[0][index-1],-112+x_offset, 160  +y_offset,1])#C1
                        components.append(['P1','',-96+x_offset, 272+y_offset, 0])#The GND label
                        wires.append([-96+x_offset, 160+y_offset, -96+x_offset, 128+y_offset])
                        wires.append([-96+x_offset, 272+y_offset, -96+x_offset, 224+y_offset])
                        index=index+1
                    else:
                        #cap is horizontal:
                        components.append(['C'+str(c_count),values[1][index-1],-64+x_offset, 112  +y_offset,0])#C1
                        wires.append([-128+x_offset, 128+y_offset, -416+x_offset, 128+y_offset])
                        wires.append([224+x_offset, 128+y_offset,  -64+x_offset, 128+y_offset])
                        index=index+1
                    c_count=c_count+1
                    x_offset=x_offset+xgrid#push next element to the right

        else:
            print('Unknown filtertype')
            return('')
        #and at last we nee the sink resistor and its wires and gnd and 'out' label
        components.append(['R2',values[0][-1],-112 +x_offset, 128+y_offset,1])#Sink resistor
        components.append(['P1','', -96+x_offset, 272+y_offset, 0])#The GND label
        wires.append([ -96+x_offset, 144+y_offset, -96+x_offset, 128+y_offset])
        wires.append([ -96+x_offset, 272+y_offset, -96+x_offset, 224+y_offset])
        #there is also a special wire needed to connect hp and bp to the sink
        if fclass=='bp':
            wires.append([-96+x_offset, 128+y_offset, -384+x_offset, 128+y_offset])
        elif fclass=='lp' and order%2==1:
            wires.append([-96+x_offset, 128+y_offset, -416+x_offset, 128+y_offset])
        elif fclass=='hp' and order%2==1:
            wires.append([-96+x_offset, 128+y_offset, -384+x_offset, 128+y_offset])
        elif fclass=='hp':
            wires.append([-96+x_offset, 128+y_offset, -416+x_offset, 128+y_offset])
        labels.append([-96+x_offset, 128+y_offset, 'Out'])

        filter=''
        for component in components:
            filter=filter+create_spice_component(component[0],component[1],component[2],component[3],component[4])
        for wire in wires:
            filter=filter+draw_wireelement(wire,'wire','spice')
        for label in labels:
            filter=filter+draw_wireelement(label,'label','spice')

        return filter
    except Exception as err:
        logging.critical(str(err),'failed in draw_spice_lum')
        pass

def draw_spice_mfb(arch,fclass,values,stagenumber,odd,labeloffset):
#this function generates all the coordinates, labels and junctions
#needed to create a mfb filter in LT spice the algorithm is very similar
#to those used in draw_spice_sk so have a look at this function for more detailed comments.
    try:
        #arch: the architecture of the filer, mfb sk or lum
        #the fclass of the filter: hp,lp,bp,bs
        #the filterorder
        #values: a list that holds the component values for each stage
        #values=values[1:]#ommit the first entry which contains the stage number
        #the grid on which to place the stages
        xgrid=848
        ygrid=496

        ygridfactor=3
        if stagenumber>0:
            ygridfactor=math.floor(stagenumber/ygridfactor)
        else:
            ygridfactor=0

        xgridfactor=0
        for _ in range(0, stagenumber):
            xgridfactor=xgridfactor+1
            if xgridfactor == 3:
                xgridfactor=0

        x_offset=xgridfactor*xgrid
        y_offset=ygridfactor*ygrid
        #a bandpass just cascades stages
        #i don't use this but it's there for completeness.
        if fclass=='bp':
            if (stagenumber+2)%2==0:
                fclass='lp'
            else:
                fclass='hp'
        #lets start with the components:
        #component grid appears to be 16 in ltspice
        components=[]
        wires=[]
        labels=[]
        #in ltspice caps and r's are diffrent length, so i cant reuse the wires.
        
        if fclass =='lp':
            if odd==0:
                components.append(['R'+str((stagenumber*3)+1),values[0],  0  +x_offset, 112+y_offset,0])#R1
                components.append(['R'+str((stagenumber*3)+2),values[1],176  +x_offset, 112+y_offset,0])#R2
                components.append(['C'+str((stagenumber*2)+1),values[3],-32  +x_offset, -16+y_offset,0])#C1
                components.append(['C'+str((stagenumber*2)+2),values[2],224  +x_offset,  32+y_offset,1])#C2
                components.append(['R'+str((stagenumber*3)+3),values[4],176  +x_offset, -16+y_offset,0])#R3
                components.append(['U'+str((stagenumber)+1)  ,'', 352+x_offset, 144+y_offset,0])#U1
                components.append(['P'+str((stagenumber)+1)  ,'', 288+x_offset, 192+y_offset,0])#GND
                components.append(['P'+str((stagenumber)+1)  ,'',-128+x_offset,   0+y_offset,0])#GND

                wires.append([ -96+x_offset,   0+y_offset, -128+x_offset,   0+y_offset])
                wires.append([  32+x_offset,   0+y_offset,  -32+x_offset,   0+y_offset])
                wires.append([  80+x_offset,   0+y_offset,   32+x_offset,   0+y_offset])
                wires.append([ 240+x_offset,   0+y_offset,  160+x_offset,   0+y_offset])
                wires.append([ 432+x_offset,   0+y_offset,  240+x_offset,   0+y_offset])
                wires.append([ 240+x_offset,  32+y_offset,  240+x_offset,   0+y_offset])
                wires.append([ -96+x_offset, 128+y_offset, -288+x_offset, 128+y_offset])
                wires.append([  32+x_offset, 128+y_offset,   32+x_offset,   0+y_offset])
                wires.append([  32+x_offset, 128+y_offset,  -16+x_offset, 128+y_offset])
                wires.append([  80+x_offset, 128+y_offset,   32+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 128+y_offset,  240+x_offset,  96+y_offset])
                wires.append([ 240+x_offset, 128+y_offset,  160+x_offset, 128+y_offset])
                wires.append([ 320+x_offset, 128+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 432+x_offset, 128+y_offset,  432+x_offset,   0+y_offset])
                wires.append([ 464+x_offset, 128+y_offset,  432+x_offset, 128+y_offset])
                wires.append([ 432+x_offset, 144+y_offset,  432+x_offset, 128+y_offset])
                wires.append([ 432+x_offset, 144+y_offset,  384+x_offset, 144+y_offset])
                wires.append([ 320+x_offset, 160+y_offset,  288+x_offset, 160+y_offset])
                wires.append([ 288+x_offset, 192+y_offset,  288+x_offset, 160+y_offset])

            elif odd==1:
                #the odd bit lets the programm create a single pole sk arch at the beginning it should not get set for any other purpose
                stage=''
                stage=draw_spice_sk(arch,fclass,values,stagenumber,odd)
                return stage
        
        elif fclass=='hp':
            if odd==0:
                components.append(['C'+str((stagenumber*3)+1+labeloffset),values[0],-32  +x_offset, 112+y_offset,0])#C1
                components.append(['C'+str((stagenumber*3)+2+labeloffset),values[1],144  +x_offset, 112+y_offset,0])#C2
                components.append(['R'+str((stagenumber*2)+1+labeloffset),values[2],  0  +x_offset, -16+y_offset,0])#R1
                components.append(['R'+str((stagenumber*2)+2+labeloffset),values[3],224  +x_offset,   0+y_offset,1])#R2
                components.append(['C'+str((stagenumber*3)+3+labeloffset),values[4],144  +x_offset, -16+y_offset,0])#C3

                components.append(['U'+str((stagenumber)+1+labeloffset)  ,'', 352+x_offset, 144+y_offset,0])#U1
                components.append(['P'+str((stagenumber)+1+labeloffset)  ,'', 288+x_offset, 192+y_offset,0])#GND
                components.append(['P'+str((stagenumber)+1+labeloffset)  ,'',-128+x_offset,   0+y_offset,0])#GND

                wires.append([ -96+x_offset,   0+y_offset, -128+x_offset,   0+y_offset])
                wires.append([  32+x_offset,   0+y_offset,  -16+x_offset,   0+y_offset])
                wires.append([  80+x_offset,   0+y_offset,   32+x_offset,   0+y_offset])
                wires.append([ 240+x_offset,   0+y_offset,  144+x_offset,   0+y_offset])
                wires.append([ 432+x_offset,   0+y_offset,  240+x_offset,   0+y_offset])
                wires.append([ 240+x_offset,  16+y_offset,  240+x_offset,   0+y_offset])
                wires.append([ -96+x_offset, 128+y_offset, -288+x_offset, 128+y_offset])
                wires.append([  32+x_offset, 128+y_offset,   32+x_offset,   0+y_offset])
                wires.append([  32+x_offset, 128+y_offset,  -32+x_offset, 128+y_offset])
                wires.append([  80+x_offset, 128+y_offset,   32+x_offset, 128+y_offset])
                wires.append([ 240+x_offset, 128+y_offset,  240+x_offset,  96+y_offset])
                wires.append([ 240+x_offset, 128+y_offset,  144+x_offset, 128+y_offset])
                wires.append([ 320+x_offset, 128+y_offset,  240+x_offset, 128+y_offset])
                wires.append([ 432+x_offset, 128+y_offset,  432+x_offset,   0+y_offset])
                wires.append([ 464+x_offset, 128+y_offset,  432+x_offset, 128+y_offset])
                wires.append([ 432+x_offset, 144+y_offset,  432+x_offset, 128+y_offset])
                wires.append([ 432+x_offset, 144+y_offset,  384+x_offset, 144+y_offset])
                wires.append([ 320+x_offset, 160+y_offset,  288+x_offset, 160+y_offset])
                wires.append([ 288+x_offset, 192+y_offset,  288+x_offset, 160+y_offset])

            elif odd==1:
                stage=''
                stage=draw_spice_sk(arch,fclass,values,stagenumber,odd)
                return stage

        elif fclass=='bs':
            if odd==0:
                components.append(['R'+str((stagenumber*5)+1),values[0],  0  +x_offset, 112+y_offset,0])#R1
                components.append(['R'+str((stagenumber*5)+2),values[1],176  +x_offset, 112+y_offset,0])#R2
                components.append(['C'+str((stagenumber*5)+1),values[2],224  +x_offset,  32+y_offset,1])#C1
                components.append(['C'+str((stagenumber*5)+2),values[3],-32  +x_offset, -16+y_offset,0])#C2
                components.append(['R'+str((stagenumber*5)+3),values[4],176  +x_offset, -16+y_offset,0])#R3

                components.append(['C'+str((stagenumber*5)+3),values[5],-32  +x_offset, 224+y_offset,0])#C3
                components.append(['C'+str((stagenumber*5)+4),values[6],160  +x_offset, 224+y_offset,0])#C4
                components.append(['R'+str((stagenumber*5)+5),values[7],  0  +x_offset, 336+y_offset,0])#R5
                components.append(['R'+str((stagenumber*5)+4),values[8],224  +x_offset, 240+y_offset,1])#R4
                components.append(['C'+str((stagenumber*5)+5),values[9],160  +x_offset, 336+y_offset,0])#C5
                

                components.append(['U'+str((stagenumber)+1)  ,'', 352+x_offset, 144+y_offset,0])#U1
                components.append(['P'+str((stagenumber)+1)  ,'', 288+x_offset, 192+y_offset,0])#GND
                components.append(['P'+str((stagenumber)+1)  ,'',-128+x_offset,   0+y_offset,0])#GND
                components.append(['P'+str((stagenumber)+1)  ,'',-128+x_offset, 352+y_offset,0])#GND

                wires.append([  -96+x_offset,   0+y_offset, -128+x_offset,   0+y_offset])
                wires.append([   32+x_offset,   0+y_offset,  -32+x_offset,   0+y_offset])
                wires.append([   80+x_offset,   0+y_offset,   32+x_offset,   0+y_offset])
                wires.append([  240+x_offset,   0+y_offset,  160+x_offset,   0+y_offset])
                wires.append([  432+x_offset,   0+y_offset,  240+x_offset,   0+y_offset])
                wires.append([  240+x_offset,  32+y_offset,  240+x_offset,   0+y_offset])
                wires.append([ -160+x_offset, 128+y_offset, -288+x_offset, 128+y_offset])
                wires.append([  -96+x_offset, 128+y_offset, -160+x_offset, 128+y_offset])
                wires.append([   32+x_offset, 128+y_offset,   32+x_offset,   0+y_offset])
                wires.append([   32+x_offset, 128+y_offset,  -16+x_offset, 128+y_offset])
                wires.append([   80+x_offset, 128+y_offset,   32+x_offset, 128+y_offset])
                wires.append([  240+x_offset, 128+y_offset,  240+x_offset,  96+y_offset])
                wires.append([  240+x_offset, 128+y_offset,  160+x_offset, 128+y_offset])
                wires.append([  320+x_offset, 128+y_offset,  240+x_offset, 128+y_offset])
                wires.append([  432+x_offset, 128+y_offset,  432+x_offset,   0+y_offset])
                wires.append([  464+x_offset, 128+y_offset,  432+x_offset, 128+y_offset])
                wires.append([  432+x_offset, 144+y_offset,  432+x_offset, 128+y_offset])
                wires.append([  432+x_offset, 144+y_offset,  384+x_offset, 144+y_offset])
                wires.append([  320+x_offset, 160+y_offset,  288+x_offset, 160+y_offset])
                wires.append([  288+x_offset, 192+y_offset,  288+x_offset, 160+y_offset])
                wires.append([ -160+x_offset, 240+y_offset, -160+x_offset, 128+y_offset])
                wires.append([  -96+x_offset, 240+y_offset, -160+x_offset, 240+y_offset])
                wires.append([   32+x_offset, 240+y_offset,  -32+x_offset, 240+y_offset])
                wires.append([   96+x_offset, 240+y_offset,   32+x_offset, 240+y_offset])
                wires.append([  240+x_offset, 240+y_offset,  240+x_offset, 128+y_offset])
                wires.append([  240+x_offset, 240+y_offset,  160+x_offset, 240+y_offset])
                wires.append([  240+x_offset, 256+y_offset,  240+x_offset, 240+y_offset])
                wires.append([  -96+x_offset, 352+y_offset, -128+x_offset, 352+y_offset])
                wires.append([   32+x_offset, 352+y_offset,   32+x_offset, 240+y_offset])
                wires.append([   32+x_offset, 352+y_offset,  -16+x_offset, 352+y_offset])
                wires.append([   96+x_offset, 352+y_offset,   32+x_offset, 352+y_offset])
                wires.append([  240+x_offset, 352+y_offset,  240+x_offset, 336+y_offset])
                wires.append([  240+x_offset, 352+y_offset,  160+x_offset, 352+y_offset])
                wires.append([  432+x_offset, 352+y_offset,  432+x_offset, 144+y_offset])
                wires.append([  432+x_offset, 352+y_offset,  240+x_offset, 352+y_offset])

            elif odd==1:
                msg='Sorry no odd order band-stop filters are possible at the moment'
                return msg
        #These are always the same, its the stagecorner and labels
        wires.append([ 480+x_offset, 128+y_offset, 464+x_offset, 128+y_offset])
        wires.append([ 560+x_offset, 128+y_offset, 480+x_offset, 128+y_offset])
        
        labels=[]
        labels.append([ 560+x_offset, 128+y_offset, 'OutStage'+str(stagenumber)])
        labels.append([ 352+x_offset, 112+y_offset, 'V+'])
        labels.append([ 352+x_offset, 176+y_offset, 'V-'])
        if stagenumber==0:
            labels.append([-288+x_offset, 128+y_offset, 'SignalIn'])
        elif xgridfactor==0:
            labels.append([ -288+x_offset, 128+y_offset, 'OutStage'+str(stagenumber-1)])
        
        stage=''
        for component in components:
            stage=stage+create_spice_component(component[0],component[1],component[2],component[3],component[4])
        for wire in wires:
            stage=stage+draw_wireelement(wire,'wire','spice')
        for label in labels:
            stage=stage+draw_wireelement(label,'label','spice')
        return stage
    except Exception as err:
        logging.critical(str(err),'failed in draw_spice_mfb')
        pass

def wptxt(Qs,values,fclass,arch,ftype,order):
#the 'write plain text function formats the data given to it to maken a nice textfile outpu
    try:
        #fclass:hp lp bp bs
        #arch: lum mfb sk
        sceme=''
        print(values)
        if order%2==1:#Set the odd bit if the filter order is uneven
            odd=1
        else:
            odd=0
        #These dictionarys contain the written out formulations of the short variable labels
        fclasses={'hp':'Highpass', 'lp':'Lowpass','bp':'Band-pass','bs':'Band-stop'}
        ftypes={'but':'Butterworth', 'bes':'Bessel','ch1':'Chebychev I','ch2':'Chebychev II','leg':'Legendre','ell':'Elliptic'}
        farches={'sk':'Sallen-Key', 'mfb':'Multiple feedback','lum':'Cauer'}
        #Creating the headlines
        sceme=sceme+'This Filter was created using IBD Filter Designer\n'
        sceme=sceme+'Filterorder: '+str(order)+'\n'
        sceme=sceme+'Filtertype: '+ftypes[ftype]+'\n'
        sceme=sceme+'Filterclass: '+fclasses[fclass]+'\n'
        sceme=sceme+'Filtertopology: '+farches[arch]+'\n'
        sceme=sceme+'Units:\n'
        sceme=sceme+"C's: [F]\n"
        sceme=sceme+"R's: [Ohm]\n"
        sceme=sceme+"L's: [Henry]\n"
        
        if arch=='lum':
            oddlabels_lp=['Stage','C','L']
            oddlabels_hp=['Stage','L','C']
        else:
            oddlabels_lp=['Stage','R','C']
            oddlabels_hp=['Stage','C','R']
        if fclass=='bs':
            labelswitch=int(len(values)+1)
            tmp=values[0][:-1]+values[1][1:]
            values=[]
            values.append(tmp)
            labelsb=[]
            if arch=='mfb':
                labelsa=['Stage','R1','R2','C1','C2','R3','C3','C4','R4','R5','C6']
            elif arch=='sk':
                labelsa=['Stage','R1','R2','C1','C2','C3','C4','R3','R4']
            elif arch=='lum':
                labelsa=['Stage','C','L','L','C']
                sceme=sceme+'Rsource : '+str(values[0][0])+'\n'
                sceme=sceme+'Rsink : '+str(values[0][-1])+'\n'
        elif fclass=='bp':
            labelswitch=int(len(values)+1)#The bandpass has as many lp's as hp's so set the halfway poin
            Qs=Qs+Qs#The Qs for high and lowpass are the same in order to use the stage as index we have to write them out
            if arch=='mfb':
                labelsa=['Stage','R1','R2','C1','C2','R3']
                labelsb=['Stage','C1','C2','R1','R2','C3']
            elif arch=='sk':
                labelsa=['Stage','R1','R2','C1','C2']
                labelsb=['Stage','C1','C2','R1','R2']
            elif arch=='lum':
                labelsa=['Stage','C','L']
                labelsb=['Stage','L','C']
                sceme=sceme+'Rsource : '+str(values[0][0])+'\n'
                sceme=sceme+'Rsink : '+str(values[0][-1])+'\n'
        elif fclass=='hp':
            labelswitch=int(len(values)+1)
            labelsb=[]
            if arch=='mfb':
                labelsa=['Stage','C1','C2','R1','R2','C3']
            elif arch=='sk':
                labelsa=['Stage','C1','C2','R1','R2']
            elif arch=='lum':
                labelsa=['Stage','L','C']
                sceme=sceme+'Rsource : '+str(values[0][0])+'\n'
                sceme=sceme+'Rsink : '+str(values[0][-1])+'\n'
        else:
            labelswitch=int(len(values)+1)
            labelsb=[]
            if arch=='mfb':
                labelsa=['Stage','R1','R2','C1','C2','R3']
            elif arch=='sk':
                labelsa=['Stage','R1','R2','C1','C2']
            elif arch=='lum':
                labelsa=['Stage','C','L']
                sceme=sceme+'Rsource : '+str(values[0][0])+'\n'
                sceme=sceme+'Rsink : '+str(values[0][-1])+'\n'
        sceme=sceme+'\n'
        for stage in range(len(values)):#values is a list of lists each list describes a stage which contains it's values
            sceme=sceme+'\n'
            #add the Stage and reference number. But not on lumped filters
            if arch=='lum':
                pass# sceme=sceme+'Stage '+str(stage)+'\n'
            else:
                sceme=sceme+'Stage '+str(values[stage][0])+'\n'

            #Odd active filters have a simple rc or cr filter in front of them
            #so they need a special label once
            if odd==1 and stage==0:
                if fclass=='hp':
                    labels=oddlabels_hp
                else:
                    labels=oddlabels_lp
            elif odd==1:#The simple RC has no Q for it in storage so shift the Q output one stage
                if arch=='lum':
                    pass
                else:
                    sceme=sceme+'Q ='+str(Qs[stage-1])+'\n'
            else:#When the filterorder is even each stage gets a Q value
                if arch=='lum':
                    pass
                else:
                    sceme=sceme+'Q ='+str(Qs[stage])+'\n'
                labels=labelsa
            
            if stage>=labelswitch:#on bandpass filters the labels have to be changed half way throu
                if odd==1 and stage==labelswitch:
                    labels=oddlabels_hp#if the filter is odd the first stage half way throu is again a single pole cr
                else:
                    labels=labelsb    
            elif stage>0 and fclass=='bp':
                labels=labelsb
            elif stage>0:
                labels=labelsa

            if arch=='lum':
                for index in range(len(values[stage])-2):
                    sceme=sceme+labels[index%2+1]+str(index)+' = '+str(values[stage][index+1])+'\n'
            else:
                for index in range(len(values[stage])-1):
                    sceme=sceme+labels[index+1]+' = '+str(values[stage][index+1])+'\n'

        return sceme
    except Exception as err:
        logging.critical(str(err),'Failed in write textfile')
        pass

def proto():
	try:
		return
	except Exception as err:
			logging.critical(str(err),'Failed in proto')
			pass

def drawScemeAP(arch, values, order, fclass, filename, tpath, app,fc,lcorner,ucorner,Qs,ftype):
#this function is the main function which is called by the Backend to draw the schematics or textfile
    try:
        #The slash is depending on the os your using so set it accordingly
        if os.name == 'nt': # Windows
            slash='\\'
        else:#Linux
            slash='/'
        cop=tpath+slash+filename#set the complete output path

        ###############################################################
        #decide which header to use eg a A3 or a custom extremly large#
        #the pagesize is hardcoded in the header files.################
        ###############################################################
        if app=='eeschema':
            headfile=open(resource_path("./media/head.txt"), "r")#open the file containing the file header
            if arch=='mfb' or arch=='sk':
                if fclass=='bs':
                    if order>18:
                        headfile=open(resource_path("./media/bighead.txt"), "r")#open the file containing the file header
                elif fclass=='bp':
                    if order>12:
                        headfile=open(resource_path("./media/bighead.txt"), "r")#open the file containing the file header
                else:
                    if order>24:
                        headfile=open(resource_path("./media/bighead.txt"), "r")#open the file containing the file header
            else:
                if fclass=='bs':
                    if order>96:
                        headfile=open(resource_path("./media/bighead.txt"), "r")#open the file containing the file header
                if fclass=='bp':
                    if order>24:
                        headfile=open(resource_path("./media/bighead.txt"), "r")#open the file containing the file header
                else:
                    if order>48:
                        headfile=open(resource_path("./media/bighead.txt"), "r")#open the file containing the file header
            sceme=headfile.read()#read its content
            headfile.close()#close it when done
        elif app=='spice':
            headfile=open(resource_path("./media/shead.txt"), "r")#open the file containing the file header
            sceme=headfile.read()#read its content
            headfile.close()#close it when done
        elif app=='ptxt':
            sceme=wptxt(Qs,values,fclass,arch,ftype,order)

        ########################################################################################
        #Next find the simulation instruction and change it to fit the given cutoff frequencys:#
        ########################################################################################
        if app=='spice' or app=='eeschema':
            if fclass=='bp' or fclass=='bp':
                sceme=sceme.replace('.ac oct 10k 1 100k','.ac oct 10k '+str(lcorner-(lcorner*0.9))+' '+str(ucorner+(ucorner*0.9)))
            elif fclass=='hp':
                sceme=sceme.replace('.ac oct 10k 1 100k','.ac oct 10k '+str(fc)+' '+str(fc*100))
            elif fclass=='lp':
                sceme=sceme.replace('.ac oct 10k 1 100k','.ac oct 10k 1 '+str(fc))
        #regardless of the architechture a single stage will always implement two
        #poles exept for odd order filters where a single pole is specially 
        #constructed.
        if order%2==1:
            odd=1
            #on odd orders an extra stage
            nostages=int(((order+1)/2))
        else:
            odd=0
            nostages=int((order/2))

        stagenumber = 0#preset the stagenumber
        labeloffset = 0
        if app=='eeschema':
            #first decission is on wheter its an active or passive filter
            if arch=='lum':
                #all lumped filters are constructed in a similar way
                #because there is only one architechture/topology
                sceme=sceme+draw_passive_stage(arch,fclass,values,order)
            else:
                if fclass=='bp':
                    if order==1:
                        return -1,'Sorry a single order band-pass not yet supported'
                    elif order%2==1:
                        odd=1
                    for _ in range(0, int(nostages)):
                        sceme=sceme+draw_active_stage(arch,'lp',values[stagenumber],stagenumber,odd,app,labeloffset)
                        stagenumber=stagenumber+1
                        odd=0
                    #reset the odd bit to draw the first stage as sk
                    if order%2==1:
                        odd=1
                    labeloffset=1
                    for _ in range(0, int(nostages)):
                        sceme=sceme+draw_active_stage(arch,'hp',values[stagenumber],stagenumber,odd,app,labeloffset)
                        stagenumber=stagenumber+1
                        odd=0
                    labeloffset = 0
                elif fclass=='bs':
                    if order%2==0:
                        for _ in range(0, nostages):
                            #the first half of the values are for the lp the last half for the hp
                            merged_values=values[stagenumber][1:]+values[nostages+stagenumber][1:]#put corresponding lowpass and a highpass together
                            sceme=sceme+draw_active_bs(arch,merged_values,stagenumber)
                            stagenumber=stagenumber+1
                    else:
                        msg='Sorry, at the moment no odd order band-stop filters are possible'
                        return msg
                else:
                    for _ in range(0, nostages):
                        sceme=sceme+draw_active_stage(arch,fclass,values[stagenumber],stagenumber,odd,app,labeloffset)
                        stagenumber=stagenumber+1
                        odd=0#if odd was one reset it since you only want to be the first stage to be a single pole
        elif app=='spice':
            if arch=='lum':
                sceme=sceme+draw_spice_lum(fclass,values,order)
            elif arch=='mfb':
                if fclass=='bp':
                    if order%2==1: #set the odd bit so the first stage is a sk real pole
                        odd=1

                    for _ in range(0, int(nostages)):
                        merged_values=values[stagenumber][1:]
                        sceme=sceme+draw_spice_mfb(arch,'lp',merged_values,stagenumber,odd,0)
                        stagenumber=stagenumber+1
                        odd=0 #reset the bit so all the following stages are normal mfb's

                    if order%2==1: #reset the bit for the first hp stage so it again is a sk
                        odd=1
                    
                    labeloffset=2

                    for _ in range(0, int(nostages)):
                        merged_values=values[stagenumber][1:]
                        sceme=sceme+draw_spice_mfb(arch,'hp',merged_values,stagenumber,odd,labeloffset)
                        stagenumber=stagenumber+1
                        odd=0#reset the bit so all the following stages are normal mfb's
                else:
                    for _ in range(0, nostages):
                        if fclass=='bs':
                            merged_values=values[stagenumber][1:]+values[nostages+stagenumber][1:]
                        else:
                            merged_values=values[stagenumber][1:]
                        sceme=sceme+draw_spice_mfb(arch,fclass,merged_values,stagenumber,odd,0)
                        stagenumber=stagenumber+1
                        odd=0

            elif arch=='sk':
                if fclass=='bp':
                    for _ in range(0, nostages):
                        merged_values=values[stagenumber][1:]
                        sceme=sceme+draw_spice_sk(arch,'lp',merged_values,stagenumber,odd)
                        stagenumber=stagenumber+1
                        #FIX
                        #this will break when odd order filters are enabled
                    for _ in range(0, nostages):
                        merged_values=values[stagenumber][1:]
                        sceme=sceme+draw_spice_sk(arch,'hp',merged_values,stagenumber,odd)
                        stagenumber=stagenumber+1
                        #FIX
                        #this will break when odd order filters are enabled
                else:
                    for _ in range(0, nostages):
                        if fclass=='bs':
                            merged_values=values[stagenumber][1:]+values[nostages+stagenumber][1:]
                        elif fclass=='bp':
                            merged_values=values[stagenumber][1:]
                        else:
                            merged_values=values[stagenumber][1:]
                        sceme=sceme+draw_spice_sk(arch,fclass,merged_values,stagenumber,odd)
                        stagenumber=stagenumber+1
                        odd=0
            else:
                print('Unknown architecture')
            pass
    
        if app=='eeschema':
            sceme=sceme+'$EndSCHEMATC'
            cop=cop+'.sch'#the ending for eeschema
        elif app=='spice':
            #LTspice has trouble with the 'µ' so replace it with a 'u'
            sceme=sceme.replace('µ','u')
            cop=cop+'.asc'#the ending for spice
        elif app=='ptxt':
            cop=cop+'.txt'#the ending for plaintxt
        
        scemefile=open(cop, "w+")#open the fie containing the file header
        scemefile.write(sceme)
        scemefile.close()
        
        if app=='eeschema':
            lib='.SUBCKT idealOpAmp   in+   in-  out  v+   v- \n'+'Gx 0 out in+ in- 10000meg\n'+'Rx out 0 1n \n'+'.ends'
            libpath=tpath+'/'+'idealOpAmp.lib'
            spicelib=open(libpath, "w+")#open the file to write the spice directives to
            spicelib.write(lib)
            spicelib.close()
        return
    except Exception as err:
        logging.critical(str(err),'Failed in createSceme')
        pass
