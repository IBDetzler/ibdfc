#!/usr/bin/python3
# -*- coding: utf-8-sig -*-
#
# IBD Filter Creator is a program to calculate a filter based on given
# parametres
# integration with the Subversion source control system.
#
# Copyright (C) 2020 by Matthias Detzler <filter@ing-detzler.de>
#
# IBD Filter Creator is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# IBD Filter Creator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with IBD Filter Creator;  If not, see <http://www.gnu.org/licenses/>.
#
# IBD Filter Creator  uses additional libraries which have their own licence
# For each such librarie you should have received a copy of their respective
# license in a file named module.license
# These modules namely are: wxPython, scipy, numpy, logging, endolith ,logging
# The modules sys, math ,cmath, getopt, os do not seem to have their own
# license so the general python license shall be applied if legal
# You should have received a copy of the GNU General Public License
# along with IBD Filter Creator;  If not, see
# <https://docs.python.org/3.7/license.html>
from scipy.signal import bessel, cheby2, ellip, buttord, cheb1ord, cheb2ord, ellipord
import numpy as np
import sys
import logging #Copyright (C) 2001-2005 by Vinay Sajip. All Rights
import math
from cmath import sqrt
from endolith import legendreap as legendre
from createSchematics import drawScemeAP



argument=str((sys.argv[0]))
logging.basicConfig(filename=argument+'.log',level=logging.WARNING)



def select_stable_poles(poles):
#this function selects the stable poles based on thei sigma value
# s= w+oj //o=sigma, w=omega
    try:
        import math
        result=[]#initialize the return value
        for complexnmbr in poles:#for each pole in the given list
            if complexnmbr.imag>0:
                result.append(complexnmbr)#only take the poles with a positive sigma
        return result
    except Exception as err:
        logging.critical(str(err),'failed in select_stable_poles')
        pass

def butter_poles (n):
#this function calculates the butterworth poles based on the given
#order n. It basically contains just the formular
    try:
        import math
        s=[]#initialize the return value
        e=math.e#its easier to read the constant in the formular if it is written this way
        pi=math.pi#its easier to read the constant in the formular if it is written this way
        start=math.ceil(n/2)#always round up
        for k in range(start+1, n+1):
            pole=-1*e**((1j*(2*k+n-1)*pi)/(2*n))
            s.append(pole)
        return s
    except Exception as err:
        logging.critical(str(err),'failed in butter_poles')
        pass

def chebI_poles(n, ripple_db):
#this function calculates the chebychev I poles based on the given
#order n and the max amount of ripple in the passband. 
#It basically contains just the formular
    try:
        import math
        e=math.e#its easier to read the constant in the formular if it is written this way
        pi=math.pi#its easier to read the constant in the formular if it is written this way
        s=[]#initialize the return value
        epsilon=math.sqrt((10**(ripple_db/10))-1)#Epsilon is part of the chebychev functions
        A=(1/n)*math.asinh(1/epsilon)#Calculate A
        Kr=math.sinh(A)#as well as its sine and cosine part
        Ki=math.cosh(A)
        #Only halv of the poles are stable but it doesn't matter which halve you take if
        #you work with the abs() so:
        start=math.ceil(n/2)#take only the last halve of the poles (they are symmetrical)
        for k in range(start+1, n+1):
            pole=-1*e**((1j*(2*k+n-1)*pi)/(2*n))
            pole=complex(pole.real*Kr, pole.imag*Ki)
            s.append(pole)
        return s
    except Exception as err:
        logging.critical(str(err),'failed in chebI_poles')
        pass

def chebII_poles(n, Satt):
#this function calculates the chebychev II poles based on the given
#order n and the Stopband attenuatione. 
#It uses the scipy function for this
    try:
        from scipy.signal import cheby2
        s=[]#initialize the return value
        epsilon=math.sqrt((10**(Satt/10))-1)#Epsilon is part of the chebychev functions
        cutoff=math.cosh((1/n)*math.acosh(epsilon)) #The -3db-frequency has to be scaled to retrieve the cutoff-frequency
        cutoff=cutoff*(-1)#this is needed to make the omega positive without this component values will get negative
        _, p, _ = cheby2(n, Satt, cutoff, 'low', analog=True, output='zpk')#use the scipy function to get the poles and throw the rest away
        s=select_stable_poles(p)#only return the stable poles
        return s
    except Exception as err:
        logging.critical(str(err),'failed in chebI_poles')
        pass

def ellip_poles(n, rip, Satt):
#this function calculates the elliptic poles based on the given
#order n , the passband ripple and the Stopband attenuatione. 
#It uses the scipy function for this
    try:
        from scipy.signal import ellip
        s=[]#initialize the return value
        _, p, _ = ellip(n, rip, Satt, -1,'low', analog=True, output='zpk')#The -1 MAkes the Q come out positive
        s=select_stable_poles(p)#only return the stable poles
        return s
    except Exception as err:
        logging.critical(str(err),'failed in ellip_poles')
        pass

def bessel_poles(n, opt):
#this function calculates the bessel poles based on the given
#order n , and the choosen normalization. 
#It uses the scipy function for this
    try:
        from scipy.signal import bessel
        import math
        #pi=math.pi
        s=[]#initialize the return value
        _, p, _ = bessel(n, -1, 'low', analog=True, norm=opt, output='zpk') #the -1 makes the resulting Q positive (optics)
        s=select_stable_poles(p)#only return the stable poles
        return s
    except Exception as err:
        logging.critical(str(err),'failed in bessel_poles')
        pass

def legendre_poles(n):
#this function calculates the legendre poles based on the given
#order n. It uses endoliths function for this.
    try:
        from endolith import legendreap as legendre
        s=[]#initialize the tmp storage value
        a=[]#initialize the return value
        _, p, _=legendre(n) #throw away anything but the poles
        s=p.tolist() #Convert it to fit the datatype of the other pole-retrieving
        for complexnmbr in s:#only take the poles with a positive sigma
            if complexnmbr.imag>0:
                #The legendre Poles with positive sigma still have a negative omega. (So do the negative sigmas)
                #So i have to get the abs from the real part or i end up with negative components
                z=complex(abs(complexnmbr.real),complexnmbr.imag)
                a.append(z)
        return a
    except Exception as err:
        logging.critical(str(err),'failed in legendre_poles')
        pass

def getQs (s):
#this function calculates the Q values as well as the frequency scaling factors
#which are part of the computation based on the given poles
#it is basically just a equation
    try:
        #define datattypes for Q and scales
        #s contains the poles
        Qs=[]#initialize the return value
        w_scales=[]#initialize the return value
        for pnumber in range (len(s)): #for every pole in the list
            w_scale=abs(s[pnumber]) #calculate the Frequency scaling factor
            Q=w_scale/(2*s[pnumber].real)# and calculate Q
            #print ("Q Stage"+str(pnumber+1)+"=",Q,"\n")
            w_scales.append(w_scale)#save the frequency scaling factor
            Qs.append(abs(Q))#also save the q
        return Qs, w_scales#resturn the results
    except Exception as err:
            logging.critical(str(err),'failed in getQs')
            pass

def getValues_LP(Qs,CV,fc,scales,ftopo):
#This function calculates the component values for a lowpass filter
#depending on the given parameters
    try:
        import math
        pi=math.pi#its easier to read the constant in the formular if it is written this way
        values=[]#initialize the return value
        stage=0#the stage counter appended to each value set
        for Q in Qs:#for every Q in the list
            value=[]#initialize the storage var
            f_scaled=scales[stage]*fc#scale the frequency
            #the calculations are diffrent for sallen key and mfb
            if ftopo=='sk':
                c_relation=4*Q**2
                r_one=CV
                r_two=CV
                r_three='open'
            elif ftopo=='mfb':
                c_relation=9*Q**2
                r_one=CV
                r_two=CV
                r_three=CV
            c_one=1/(2*pi*r_one*f_scaled*math.sqrt(c_relation))
            c_two=c_relation*c_one
            #append the calculated calues in the right order
            #to keep returned values consisten also add r_one, r_two and r_three in the
            #same way as in the HP calculation
            value.append(stage)
            value.append(r_one)
            value.append(r_two)
            value.append(c_one)
            value.append(c_two)
            if ftopo=='mfb':#for mfb filter also apend r3
                value.append(r_three)
            values.append(value)#add the values to the returned values list
            stage=stage+1#increase the stage counter with each calculatred Q
        return values
    except Exception as err:
            logging.critical(str(err),'Failed  to retrieve Values for Cs')
            print ('Failed in getValues_LP')
            pass

def getValues_HP(Qs,CV,fc,scales,ftopo):
#This function calculates the component values for a highpass filter
#depending on the given parameters. See the lowpass function for a bit more
#details
    try:
        import math
        pi=math.pi
        values=[]
        stage=0
        for Q in Qs:
            value=[]
            f_scaled=fc/scales[stage]
            if ftopo=='sk':
                r_relation=4*Q**2
                r_relation=1/r_relation
                r_two=CV
                r_one=r_two*r_relation
                c_one=1/(2*pi*f_scaled*r_two*math.sqrt(r_relation))
            elif ftopo=='mfb':
                r_relation=9*Q**2
                r_relation=1/r_relation
                r_one=CV
                r_two=r_one*r_relation
                c_one=1/(2*pi*f_scaled*r_one*math.sqrt(r_relation))
            c_two=c_one
            c_three=c_one#*2
            #there are two places where these can be set either in the Lowpass or the highpass
            #depending on what you choose to build
            #the output is respectivly twisted in the same way the components are
            value.append(stage)
            value.append(c_one)
            value.append(c_two)
            value.append(r_two)
            value.append(r_one)
            if ftopo=='mfb':
                value.append(c_three)
            values.append(value)
            stage=stage+1
        return values
    except Exception as err:
            logging.critical(str(err),'Failed  to retrieve Values for Cs')
            pass

def getRsk(Qs,C1,C2,fc,scales):
#this function was meant to be used for calculations with
#constant C values but this is now or at least for the moment
#obsolete and/or unused
    try:
        import math
        pi=math.pi
        stage=0
        values=[]
        for Q in Qs:
            value=[]
            f_scaled=scales[stage]*fc
            #print ("Frequency Scaling Factor in Stage"+str(stage)+"=",scales[stage],"\n")
            r_relation= ( -2*Q**2 - sqrt(1 - 4*Q**2) + 1 ) / 2*Q**2
            r_relation=r_relation.real
            r_two=1/(2*pi*C1*f_scaled*math.sqrt(r_relation))
            r_one=r_two*r_relation
            #print ("R1 in Stage"+str(stage)+"=",r_one,"\n")
            #print ("R2 in Stage"+str(stage)+"=",r_two,"\n")
            value.append(stage)
            value.append(r_one)
            value.append(r_two)
            stage=stage+1
        return values
    except Exception as err:
            logging.critical(str(err),'Failed to retrieve Values for Rs')
            pass

def cEEn(chain):
#Looks for SI prefixes and converts the input to a float
    try:
        nothingfound=1#this gets set to false if something was found in the chain
        chain=str(chain)#make sure the given var is a string type
        #this is a dict with the SI prefixes and their respective factor
        prefixes={ 'Y':10**24,'Z':10**21,'E':10**18,'P':10**15,'T':10**12,'G':10**9,'M':10**6,'Meg':10**6,'k':10**3,
                    'h':10**2,'da':10**1,'d':10**-1,'c':10**-2,'m':10**-3,'u':10**-6,'µ':10**-6,'n':10**-9,
                    'p':10**-12,'f':10**-15,'a':10**-18,'y':10**-21,'z':10**-24}
        #now look in the chain value if there is any mathing string in the dict
        for prefix in prefixes:
            if prefix in chain:#if you find something
                if chain.find(prefix)==(len(chain)-1):#if it is the last char in the string
                    chain=chain.replace(prefix,'')#, just strip it
                else:
                    chain=chain.replace(prefix,'.')#if its not the last char replace it with a '.'
                chain=float(chain)*prefixes.get(prefix)#multiplie the given string by the value stored in the dictionary
                nothingfound=0#mark that i found something
                break
            else:
                pass#if nothing is found just pass
        if nothingfound==1: #Make shure to return a valid floating number if no Si prefix was found
            number=''.join(c for c in chain if c.isdigit() or '.' or ',')# Strip all characters but leve ',' and '.' alone
            chain=float(number)#convert it to a float
        return chain
    except Exception as err:
            logging.critical(str(err),'cEEn failed to Convert the string to something Usefull')
            pass

def getValues_active(CV, Qs, scales, fc, ucorner, lcorner, fclass, ftopo,order):
#this function gathers the values needed for each stage and putting them in a nice format
    try:
        values=[]#initialize the return value
        valuesLP=[]#initialize the tmp storage
        valuesHP=[]#initialize the tmp storage
        if order%2==0:
            pass
        else:
            #if the order is odd append a real pole
            #stage at the beginning of every valueset
            values.append(getSimpleRC(fc,CV,fclass))
            valuesLP.append(getSimpleRC(fc,CV,'lp'))
            valuesHP.append(getSimpleRC(fc,CV,'hp'))
        if fclass=='lp':#get lowpass values
            values=values+getValues_LP(Qs,CV,fc,scales,ftopo)
        elif fclass=='hp':#get highpass values
            values=values+getValues_HP(Qs,CV,fc,scales,ftopo)
        elif fclass == 'bp':#get bandpass values
            valuesLP=valuesLP+getValues_LP(Qs,CV,ucorner,scales,ftopo)
            valuesHP=valuesHP+getValues_HP(Qs,CV,lcorner,scales,ftopo)
            values=valuesLP+valuesHP
        elif fclass =='bs':#get bandstop values
            valuesLP=getValues_LP(Qs,CV,lcorner,scales,ftopo)
            valuesHP=getValues_HP(Qs,CV,ucorner,scales,ftopo)
            values=valuesLP+valuesHP
        return values
    except Exception as err:
            logging.critical(str(err),'Failed in make_sk')
            pass

def getValues_lum(poles,timp,fc,scales,order,fclass):
#this function calculates the values for lumped LC filters
#based on the given parameters
    try:
        odd=0
        odd=order%2#set the odd bit on odd order filters
        values=[]
        values.append(timp)#First entry is the source resistance
        pi=math.pi
        cnt=0
        if order==1:
            values.append(1/(2*pi*fc*timp))#on single order poles use a RC
        else:#in any other case realize a LC filter
            for pole in poles:#every pole yield a value pair
                value=pole.real/(pi*fc*scales[cnt])
                values.append(value/timp)#append the C value
                values.append(value*timp)#append the L value
                cnt=cnt+1#count up the frequency scale factor
            if odd==1:#odd order filters get a pole mirrored in
                #FIX
                #I am not sure on which pole to mirror on odd orders...
                value=poles[-1].real/(pi*fc)
                values.append(value*timp)
                values[-2]=values[-2]*2
            else:
                pass
        values.append(timp)#Last entry is the sink resistance
        return values
    except Exception as err:
            logging.critical(str(err),'Failed in getValues_lum')
            pass

def getValues_passive(poles,timp,fc, ucorner, lcorner,scales,order,fclass):
#this function gathers the values needed for each stage and putting them in a nice format
#much like getVAlues_active
    try:
        values=[]
        valuesHP=[]
        valuesLP=[]
        #FIX
        #decide if you want the source and sink resistance get their own list
        #or once in every list
        if fclass=='bs':
            valuesHP=getValues_lum(poles,timp,ucorner,scales,order,'hp')
            valuesLP=getValues_lum(poles,timp,lcorner,scales,order,'lp')
            values.append(valuesLP)
            values.append(valuesHP)
        elif fclass=='bp':
            valuesHP=getValues_lum(poles,timp,lcorner,scales,order,'hp')
            valuesLP=getValues_lum(poles,timp,ucorner,scales,order,'lp')
            values.append(valuesLP)
            values.append(valuesHP)
        else:
            values.append(getValues_lum(poles,timp,fc,scales,order,fclass))
        return values
    except Exception as err:
            logging.critical(str(err),'Failed in getValues_passive')
            pass

def getN(fc,fs,att,ftype,fclass,rip,lcorner,ucorner):
#This function calculates the minimum required order by the given parameters.
#it is called directly from the frontend and isn't used anywhere else
    try:
        order=0
        #####################################################
        #first convert all given values to machine readables#
        #####################################################
        fc=cEEn(fc)#Cutoff freq
        ucorner=cEEn(ucorner)#upper corner freq
        lcorner=cEEn(lcorner)#lower corner freq
        fs=cEEn(fs)#Stopband freq
        rip=cEEn(rip)#Cutoff freq
        att=cEEn(att)#Cutoff freq
        lookup=getDict()#get the lookuptable from the dictionary in getDict()
        ftype=lookup[0][ftype]#lookup the class type and topo to make them human readable
        fclass=lookup[2][fclass]#lookup the class type and topo to make them human readable
        if att>3000:#The value gets too big and python/scipy throws an overflow error if att is too big
            return -1, "Sorry, the engine can't handle such high numbers. You can still increase n on your own if you'd like"
        #pass the frequency values to the right places
        if fclass=='lp':
            wp=fc
            ws=fs
        elif fclass=='hp':
            wp=fc
            ws=fs
        elif fclass=='bp':
            wp=[ucorner,lcorner]
            ws=[fs,fs]
        elif fclass=='bs':
            wp=[lcorner,ucorner]
            ws=[fs,fs]
        if ftype=='but' or ftype=='leg' or ftype=='bes':
            order,_=buttord(wp, ws, 3.0, att, analog=True, fs=None)
        elif ftype=='ch1':
            order,_=cheb1ord(wp, ws, 3, att, analog=True, fs=None)
        elif ftype=='ch2':
            order,_=cheb2ord(wp, ws, 3, att, analog=True, fs=None)
        elif ftype=='ell':
            order,_=ellipord(wp, ws, 3, att, analog=True, fs=None)

        return 0, str(order)
    except Exception as err:
            if str(err)=='The lower bound exceeds the upper bound.':
                err='Please set the stop band frequency somewhere in between the lower and the upper corner frequency.'
            return -1, str(err)

def getSatt(fc,fs,order,ftype,fclass,rip,lcorner,ucorner):
#This function estimates the Stopband attenuation by the given parameters.
#it is called directly from the frontend and isn't used anywhere else
    try:
        att=0#Stopband Attenuation
        #####################################################
        #first convert all given values to machine readables#
        #####################################################
        fc=cEEn(fc)#Cutoff freq
        fs=cEEn(fs)#Stopband freq
        order=cEEn(order)#Filterorder
        ucorner=cEEn(ucorner)#upper corner freq
        lcorner=cEEn(lcorner)#lower corner freq
        rip=cEEn(rip)#Cutoff freq
        lookup=getDict()#get the lookuptable from the dictionary in getDict()
        ftype=lookup[0][ftype]#lookup the class type and topo to make them human readable
        fclass=lookup[2][fclass]#lookup the class type and topo to make them human readable
        if order>300:
            return -1, "Sorry, the engine can't handle such high numbers. You can still increase n on your own if you'd like"

        if fclass=='lp':
            r=fs/fc
        elif fclass=='hp':
            r=fc/fs
        elif fclass=='bp':
            #take whatever is steeper:
            if lcorner/fs > fs/ucorner:
                r=lcorner/fs
            else:
                r=fs/ucorner
        elif fclass=='bs':
            #take whatever is steeper:
            if fs/lcorner>ucorner/fs:
                r=fs/lcorner
            else:
                r=ucorner/fs


        if ftype=='but':
            att=10*math.log(1+((r)**(order)))
        elif ftype=='bes':
            att=6+math.log(r)*order*6#its roughly about 6db per element and octave
        elif ftype=='leg':
            #FIX
            #The same as a butterworth for now
            att=10*math.log(1+((r)**(order)))
        elif ftype=='ch1':
            #FIX
            #'A' is at best a crude approximation
            epsilon=math.sqrt((10**(rip/10))-1)
            A=3**(order)#(1/order)*math.asinh(1/epsilon)
            B=(1/order)*math.acosh(1/epsilon)
            att=10*math.log(1+(epsilon**2)*(A**2)*(r)*B)
        elif ftype=='ch2':
            #FIX
            #The same as a butterworth for now
            att=10*math.log(1+((r)**(order)))
        elif ftype=='ell':
            #FIX
            #'A' is at best a crude approximation
            #the calculation is also taken from chebI
            epsilon=math.sqrt((10**(rip/10))-1)
            A=3**(order)#(1/order)*math.asinh(1/epsilon)
            B=(1/order)*math.acosh(1/epsilon)
            att=10*math.log(1+(epsilon**2)*(A**2)*(r)*B)

        att=int(math.floor(att))#because its only a rough approximation floor and make it an int
        return 0,str(att) #return the result
    except Exception as err:
            return -1, str(err)

def getSimpleRC(fc,CV,fclass):
#this function implements a classic RC/CR filter to get a single pole element
    try:
        #since CV is either a cap or a R this does calculate true for both
        #hp and lp
        pi=math.pi
        values=[]
        values.append(0)#keept it consisten append the stagenumber
        v=(1/(2*fc*pi*CV))#the calcutlated value for either R or C
        #keep the structure consistent to the other calculations
        if fclass=='lp':
            values.append(CV)
            values.append(v)
        if fclass=='hp':
            values.append(v)
            values.append(CV)
        return values
    except Exception as err:
            logging.critical(str(err),'Failed in get simple rc')
            pass

def sanitycheck(fclass,ftype,ftopo,op,order,fc,pAtt,att,rip,CV,lcorner,ucorner,timp,tname,kicad,spice,ptxt,tpath):
#check the given parameter for anything unreasonable
    try:
        args=[fclass,ftype,ftopo,op,order,fc,pAtt,att,rip,CV,lcorner,ucorner,timp,tname,kicad,spice,ptxt,tpath]
        for key in args:#check if all fields contain values
            if key is None:
                return -1, 'Please fill out all Entry fields.\n'
        odd=int(order)%2
        if int(order) ==0:
            return -1, 'Please choose a order grater than 0'
        elif odd==1 and fclass=='bp':
            return -1, 'Sorry, odd order Band-pass filter are not yet supported'
        elif odd==1 and fclass=='bs':
            return -1, 'Sorry, odd order Band-stop filter are not yet supported'
        elif lcorner>ucorner:
            return -1, 'The lower corner frequency must be greater than the upper corner frequency'
        elif int(kicad)==0 and int(spice)==0 and int(ptxt)==0:
            return -1, 'Please choose a output format'
        else:
            return 1, ''
    except Exception as err:
            logging.critical(str(err),'Failed in sanitycheck')
            return -1, 'Failed in sanitycheck: \n' + str(err)

def ctEE(nmbr,digits):
#this function rounds the given number to a given number of digits and
#converts it to the engineering / SI notation
    try:
        nmbr='%e' % float(nmbr)#Convert the given nmbr into scientific notation
        nmbr=str(nmbr)#Make it a string
        nmbr_bu=nmbr#make a backup of the original number
        nmbr_exp=nmbr[-4:]#cut the exponent and store it
        if nmbr_exp[0]=='e':#check if it is indeed sci notation
            nmbr=float(nmbr[:-4])
        else:#abort conversion : no scientific notation
            return float(nmbr)
        #A dict with the SI prefixes
        prefixes={    'e+24':'Y', 'e+21':'Z', 'e+18':'E', 'e+15':'P', 'e+12':'T', 'e+09':'G', 'e+06':'Meg', 'e+03':'k',
                    'e+02':'h', 'e+01':'da', 'e-01':'d', 'e-02':'c', 'e-03':'m', 'e-06':'µ', 'e-09':'n',
                    'e-12':'p', 'e-15':'f', 'e-18':'a', 'e-21':'y', 'e-24':'z', 'e+00':' '}
        tens=float(nmbr_exp[1:])# the sci notation is on base ten
        if tens<3 and tens>=0:#for 3 to -3 there are matching SI prefixes but i won't use them
            exp_n='%d' % round(float(nmbr_bu),digits)#use the number backup to get the result
            result=str(exp_n)
        else:
            thousands=tens/3# all SI's outside 3 to -3 increase on the base of thousand so get the closest
            thousands=int(math.floor(thousands)) #floor the result to get the lower exponent
            exp_n=thousands*3#convert it to the base of ten again to find the right match in the dict
            adj=tens-exp_n#we change the exponent so we have to adjust the value as well
            nmbr=round(nmbr*(10**adj),digits)#adjust and also round it

            if 10>exp_n>0:#this is to compensate for the '-' and the '0'
                exp_n='e+0'+str(exp_n)
            elif 0>exp_n>-10:
                exp_n='e-0'+str(abs(exp_n))
            elif exp_n>0:
                exp_n='e+'+str(exp_n)
            else:
                exp_n='e'+str(exp_n)
            result=str(nmbr)+prefixes[exp_n]#return the adjusted number and correct si prefix as string
        return result
    except Exception as err:
            logging.critical(str(err),'Failed in make_sk')
            pass

def proto():
#a prototype dummy for new functions
    try:
        return
    except Exception as err:
            logging.critical(str(err),'Failed in make_sk')
            pass

def getDict():
#just a set of Dictionarys to "translate" the inputs"
    ftypes = {        1:  'bes' ,
                    0:  'but' ,
                    5:  'leg' ,
                    2:  'ch1' ,
                    3:  'ch2' ,
                    4:  'ell' }

    topologies = {  0:  'sk'  ,
                    1:  'mfb' ,
                    2:  'lum' }

    fclasses =     {     0:  'lp'  ,
                    1:  'hp'  ,
                    2:  'bp'  ,
                    3:  'bs'  }
    look=[ftypes, topologies, fclasses]
    return look

def netlists(fclass,ftype,ftopo,op,order,fc,fs,att,rip,CV,lcorner,ucorner,timp,tname,kicad,spice,ptxt,tpath):
#this is the main Backend function it manages the values passed down to the Backend and calls the drawer
    try:
        emsg=''
        err=0
        #get the lookuptable from the dictionary in getDict()
        lookup=getDict()
        #lookup the class type and topo to make them human readable
        fclass=lookup[2][fclass]
        ftype=lookup[0][ftype]
        ftopo=lookup[1][ftopo]

        #####################################################
        #Convert the values passed down as string to floats:#
        #####################################################
        fc=cEEn(fc)
        CV=cEEn(CV)
        lcorner=cEEn(lcorner)
        ucorner=cEEn(ucorner)
        att=cEEn(att)
        fs=cEEn(fs)
        rip=cEEn(rip)
        timp=cEEn(timp)
        order=int(cEEn(order))


        ##################################################################################
        #check the parameters to be in a valid range and send abort message if they don't#
        ##################################################################################
        sane, emsg=sanitycheck(fclass,ftype,ftopo,op,order,fc,fs,att,rip,CV,lcorner,ucorner,timp,tname,kicad,spice,ptxt,tpath)
        if sane==1:
            pass
        else:
            return -1, emsg

        #Debug Information
        # print('Type=', ftype,' ',type(ftype))
        # print('Topo=', ftopo,' ',type(ftopo))
        # print('Class=', fclass,' ',type(fclass))
        # print('op=', op,' ',type(op))
        # print('Order=', order,' ',type(order))
        # print('Cutoff=', fc,' ',type(fc))
        # print('patt=', pAtt,' ',type(pAtt))
        # print('satt=', att,' ',type(att))
        # print('rip=', rip,' ',type(rip))
        # print('CV=', CV,' ',type(CV))
        # print('lcorner=', lcorner,' ',type(lcorner))
        # print('ucorner=', ucorner,' ',type(ucorner))
        # print('timp=', timp,' ',type(timp))
        # print('name=', tname,' ',type(tname))
        # print('kicad=', kicad,' ',type(kicad))
        # print('spice=', spice,' ',type(spice))
        # print('plaintxt=', ptxt,' ',type(ptxt))
        # print('tpath=', tpath,' ',type(tpath))

        #################################################################################
        #Select which TYpe of Filter was choosen and Calculate corresponding Pole values#
        #################################################################################
        a=[]
        if ftype == 'bes':
            #This decodes the given normalisation parameter for Bessel Filters
            if op==0:op='phase'    #Bessel normalisation either magnitude phase or delay eg 'phase', 'mag' or 'delay'
            elif op==1:op='mag'
            else: op='delay'
            a=bessel_poles(order, op)
        elif ftype == 'but':
            a=butter_poles(order)
        elif ftype == 'leg':
            a=legendre_poles(order)
        elif ftype == 'ch1':
            a=chebI_poles(order, rip)
        elif ftype == 'ch2':
            a=chebII_poles(order, att)
        elif ftype == 'ell':
            a=ellip_poles(order, rip, att)

        #########################################################################
        #The Q- and Scaling Factors are then calculated based on the Pole values#
        #########################################################################
        Qs, scales=getQs(a)
        if ftype == 'leg':#The scales and Qs come out biggest value first in legendre
            Qs.sort()
            scales.sort()
        #check the Qs for exceptionally high values:
        for Q in Qs:
            if Q>25:
                err=-1
                emsg=emsg+'[Warning] The Filter you created has very high Q values. You will most likely experience excessive gain ripple.\n'
                break

        ############################################
        #Lumped bandpasses/stops are suboptimal atm#
        ############################################
        if ftopo == 'lum' and (fclass=='bp' or fclass=='bs'):
            err=-1
            emsg=emsg+'[Warning] There is only one topology for lumped filters available at the moment which is, '
            emsg=emsg+'depending on your parameters, not well suited for all band-pass and band-stop filters especially '
            emsg=emsg+'on higher orders. If you rearrange the components and or re scale the edge-frequencys you may get a much better result.\n'

        #################################################################################################
        #Dependent on the choosen architechure and filterclass the values for R are then calculated#
        #################################################################################################
        if ftopo == 'lum':
            print('Poles: ',a)
            values=getValues_passive(a,timp,fc, ucorner, lcorner,scales,order,fclass)
        elif ftopo == 'mfb' or 'sk':
            values=getValues_active(CV, Qs, scales, fc, ucorner, lcorner, fclass, ftopo, order)

        #############################################################
        #round the numbers to x digits and convert it to SI notation#
        #############################################################
        digits=4
        #digits='{:0.'+str(digits)+'e}'
        stage=0
        nota=0
        notb=0
        for pack in values: #The format can be a list or a list of lists
            index=0
            if type(pack)==list:
                for _ in pack:
                    #check the values for very large or small values:
                    if values[stage][index]>1e+9 and nota==0 and index>0:
                        err=-1
                        emsg=emsg+'[Warning] Large component value in stage '+str(stage)+' detected\n'
                        nota=1
                    if values[stage][index]<7e-12 and notb==0 and index>0:
                        err=-1
                        emsg=emsg+'[Warning] Very small component value in stage '+str(stage)+' detected\n'
                        notb=1
                    values[stage][index]=ctEE(values[stage][index],digits)
                    index=index+1
            else:
                values[stage]=ctEE(values[stage][index],digits)
                #values[stage]=float(digits.format(values[stage]))
            stage=stage+1
            nota=0
            notb=0
        #print(values)

        #########################################
        #Draw the Output in the choosen formats:#
        #########################################
        if spice==1:
            #print('creating spice netlist')
            drawScemeAP(ftopo, values, order, fclass, tname, tpath, 'spice',fc, lcorner, ucorner,Qs,ftype)
            #print('done!')
        if kicad==1:
            #print('creating EEschema scematic')
            drawScemeAP(ftopo, values, order, fclass, tname, tpath, 'eeschema',fc, lcorner, ucorner,Qs,ftype)
            #print('done!')
        if ptxt==1:
            #print('creating plaintext file')
            drawScemeAP(ftopo, values, order, fclass, tname, tpath, 'ptxt',fc, lcorner, ucorner,Qs,ftype)
            #print('done!')
        if err==-1:
            return -1, emsg
        else:
            return 0, 'Success'

    except Exception as err:
            logging.critical(str(err),'Failed in the main Backend Function')
            print (err)
            print ('Failed in the main Backend Function')
            return -1, err

#This is for debugging without a frontend
# detailed=0
# if detailed==1:
#     for fclass in range(0,4):
#         msg=['lp','hp','bp','bs']
#         msg=msg[fclass]
#         for order in range (0,24):
#             err,emsg=netlists(fclass,0,0,1,order,'1k',3.0,60,1,'1k','1k','2k',1,'MyFilter'+str(fclass)+' '+msg+str(order),1,1,1,'/home/mr87/Documents/code/python/IBD Filter Tool/sandbox/debugOut/sk')
#             if err==-1:
#                 print(emsg)
#             err,emsg=netlists(fclass,0,1,1,order,'1k',3.0,60,1,'1k','1k','2k',1,'MyFilter'+str(fclass)+' '+msg+str(order),1,1,1,'/home/mr87/Documents/code/python/IBD Filter Tool/sandbox/debugOut/mfb')
#             if err==-1:
#                 print(emsg)
#             err,emsg=netlists(fclass,0,2,1,order,'1k',3.0,60,1,'1k','1k','2k',1,'MyFilter'+str(fclass)+' '+msg+str(order),1,1,1,'/home/mr87/Documents/code/python/IBD Filter Tool/sandbox/debugOut/lum')
#             if err==-1:
#                 print(emsg)
# else:
#     fclass=2 # 0-lp  1-hp 2-bp 3-bs
#     ftype=5  # 0-but 4-ell 5-leg
#     ftopo=2  # 0-sk 1-mfb 2-lum
#     op=0
#     order='8'
#     fc='1k'
#     fs='10k'
#     att='60'
#     rip='1'
#     CV='10k'
#     lcorner='1k'
#     ucorner='20k'
#     timp='50'
#     tname='MyFilter'
#     kicad=1
#     spice=1
#     ptxt=1
#     tpath='/home/mr87/Documents/code/python/IBD Filter Tool/sandbox/debugOut'
#     err,msg=netlists(fclass,ftype,ftopo,op,order,fc,fs,att,rip,CV,lcorner,ucorner,timp,tname,kicad,spice,ptxt,tpath)
#     if err==-1:
#                 print(msg)
